 <?php
	$all_categories = get_categories();

	$args = array(
		'posts_per_page' => 7,
		'offset' => 0,
		'category' => 0,
		'orderby' => 'post_date',
		'order' => 'DESC',
		'include' => '',
		'exclude' => '',
		'meta_key' => '',
		'meta_value' => '',
		'post_type' => 'review',
		'post_status' => 'publish',
		'suppress_filters' => true
	);

	$recent_posts = new WP_Query($args, ARRAY_A);
	// $recent_posts = wp_get_recent_posts( $args, ARRAY_A );

	?>
 <!-- pageheader
    ================================================== -->
 <?php get_header(); ?>

 <div class="colorlib-loader"></div>

 <div id="page">
 	<div class="container-fluid">
 		<div class="row">
 			<div class="col-md-12 bg-deraz-dark  top-about-section breadcrumbs text-center">
 				<h2 class="text-white bg-deraz" style="width: auto;padding: 1rem;"><?= ucwords(the_title()) ?></h2>
 				<p class="text-white"><span><a href="<?= get_bloginfo('url'); ?>">Home</a></span> / <span><a href="/community/reviews"><?= __('Reviews') ?> </a></span> / <span><?= the_title() ?></span></p>
 			</div>
 		</div>
 	</div>

 	<div id="colorlib-container">
 		<div class="container">
 			<div class="row">
 				<div class="col-md-9 content">
 					<div class="row row-pb-lg">
 						<?php
							if (have_posts()) :
								while (have_posts()) : the_post();
									?>
 						<div class="col-md-12">
 							<div class="blockquote mb-0 bg-white p-3 my-3">
 								<h3 class="review-title"><?= ucwords(get_the_title()); ?></h3><br> <?php print_stars(get_field('stars')) ?>
 								<p class="border-left-deraz-qoute" style="min-height:221px">"<?= get_the_excerpt() ?>"</p>
 								<div class="blockquote-footer" style="min-height:40px;">
 									<img class="img-fluid d-inline-block rounded-circle mx-2" src="<?= get_field("author_image") ?>"><b><?= get_field('author') ?></b> at <span class="date"><?=the_date("Y-m-d"); ?></span></div>
 							</div>
 						</div>
 						<?php
								endwhile;
							endif;
							?>
 					</div>

 				</div>
 				<div class="col-md-3">
 					<div class="sidebar">
 						<div class="side">
 							<div class="form-group">
								 <form action="<?= get_bloginfo('url'); ?>" type="get">
									<input type="text" name="s" class="form-control" id="search" placeholder="<?=__("Type here to search ...") ?>">
									<button type="submit" class="btn btn-primary"><i class="icon-search3"></i></button>
								</form>
 							</div>
 						</div>
 						<!-- <div class="side">
								<h2 class="sidebar-heading">Categories</h2>
								<p>
									<ul class="category">
									<?php
									foreach ($all_categories as $cat) {
										echo '<li><a href="' . get_category_link($cat) . '"><i class="icon-check"></i>' . $cat->name . '</a></li>';
									}
									?>
									</ul>
								</p>
							</div> -->
 						<div class="side">
 							<h2 class="sidebar-heading"><?= __('recent-reviews') ?></h2>
 							<div class="f-blog">
 								<?php
									if ($recent_posts->have_posts()) :
										while ($recent_posts->have_posts()) :
											$recent_posts->the_post();
											$bg =  wp_get_attachment_url(get_post_thumbnail_id());
											?>
 								<div class="desc my-3">
 									<h3>
 										<a href="<?= the_permalink(); ?>">
 											<div class="row">
 												<!-- <div class="col-lg-4">
																<?= get_the_post_thumbnail(null, array('80', '80')); ?>
															</div> -->
 												<div class="col-lg-12">
 													<?= the_title(); ?><br>
 													<small><?= the_date("Y-m-d"); ?></small>
 												</div>
 											</div>
 										</a>
 									</h3>
 									<hr>
 								</div>
 								<?php
										endwhile;
									endif;
									?>
 							</div>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 	</div>

 	<!-- s-footer
    ================================================== -->
 	<?php get_footer(); ?>