<?php
/*
Template Name: HomePage
*/
?>
<?php get_header(); ?>
<!-- <div class="featured"> -->
<?php
//  echo do_shortcode('[smartslider3 slider=1]'); 
?>
<!-- </div> -->
<!--Header-area-->
<header class="bg-deraz-dark">
  <?php echo do_shortcode('[smartslider3 slider=5]'); ?>
</header>
<!--Header-area/-->

<section class="container-fluid about skew bg-deraz text-white">
  <!-- <div class="col-11 offset-1"> -->
  <div class="row top">
    <!-- <div class="col-lg-6 offset-lg-1 info align-self-center"> -->
    <div class="col-lg-6 col-md-12 info align-self-center">
      <div class="row">
        <div class="col-md-3 <?= pll_current_language() == "en" ? "offset-md-1" : ""; ?> col-sm-4 ">
          <?php if (pll_current_language() == "ar") : ?>
            <h1 class="section-title text-white ">ما <br>عملنا ؟</h1>
          <?php else : ?>
            <h1 class="section-title text-white ">What <br>we do?</h1>
          <?php endif; ?>
        </div>
        <div class="col-md-8 col-sm-8 text-justify">
          <?php if (pll_current_language() == "ar") : ?>
            <p>دراز كورنر هي أكثر من مجرد مساحة للدراسة والعمل، بل هو مجتمع كامل وبيئة إبداعية لها رؤيتها وطريقتها في المساهمة الفعالة والتغيير الإيجابي. يضم مجتمع الكورنر شباب بخلفيات مختلفة، يتشاركون المعرفة، يصنعون التجارب والكثير من الذكريات..</p>
          <?php else : ?>
            <p>Deraz Corner is more than just a workspace with offices,desks and chairs. It's a creative hub with an inspiring environment, rather a community with a distinct vision for a positive change. Our corner community contains youth with different backgrounds, Sharing knowledge thriving, experiences,and make memories together.</p>
          <?php endif; ?>
        </div>
      </div>

    </div>
    <div class="col-lg-6 col-md-12 join float-left bg-deraz-dark">
      <div class="row">
        <div class="col-md-6">

          <div id="fb-root"></div>
          <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3&appId=284231908444266&autoLogAppEvents=1"></script>

          <div class="fb-video" data-href="https://www.facebook.com/SLEIDSE/videos/606725483004825/" data-width="500" data-show-text="false"  >
            </blockquote>
          </div>

        </div>
        <div class="col-md-6 my-auto">
          <?php if (pll_current_language() == "ar") : ?>
            <a href="https://my.corner.deraz.ly/login">
              <h1 class="text-white section-title"><span><b>كن</b></span> <br>جزءا من منا <span style="color:#64cb82;">></span></h1>
            </a>
          <?php else : ?>

            <a href="https://my.corner.deraz.ly/login">
              <h1 class="text-white section-title"><span><b>be</b></span> <br>one of us <span style="color:#64cb82;">></span></h1>
            </a>
          <?php endif; ?>

        </div>
      </div>
    </div>
  </div>

  <!-- activities -->
  <div class="row">
    <div class="container activities">
      <div class="col-lg-10 col-md-12 offset-lg-1 bg-deraz-dark black text-center">
        <h2 class="text-white"> <span><img src="<?php echo get_bloginfo('template_directory'); ?>/images/calendar.svg" alt=""></span>
          <?php if (pll_current_language() == "ar") : ?>
            نشاطات <b>الكورنر</b>
          <?php else : ?>
            <b>corner</b> activities
          <?php endif; ?>
        </h2>
      </div>
      <div class="inner bg-white">
        <?php
        $events_req  = wp_remote_get(get_api_url("events"));
        if (is_wp_error($events_req)) {
          $events = [];
        } else {
          $events = wp_remote_retrieve_body($events_req);
          $events = json_decode($events_req["body"])->data;
        }

        if (count($events)) :
          $events = $events[0];
          ?>
          <div class="row">
            <div class="col-md-6">
              <h2><?= __('next-event') ?></h2>
              <h4><?= $events->{get_attr_in_lang("title")} ?></h4>
              <p><b><?= __('instructor') ?></b> : <?= $events->instructor ?></p>
              <?php
                $date = new DateTime($events->start_date);
                $time = new DateTime($events->start_time);
                ?>
              <p class="time"> <i class="far fa-calendar-alt"></i> <?= $date->format('d-m-Y'); ?> <i class="far fa-clock"></i> <?= $time->format('H:i'); ?></p>
            </div>
            <div class="col-md-6">
              <p><?= $events->{get_attr_in_lang("description")} ?></p>
              <?php if ($events->reg_link == "#") : ?>
                <p><a href="#"></a></p>
              <?php else : ?>
                <p><a href="<?= $events->reg_link; ?>" target="_blank" rel="<?= $events->title_en ?>"><?= __("register"); ?></a></p>
              <?php endif; ?>
            </div>
          </div>
        <?php else : ?>
          <h2>NO events for now </h2>
        <?php endif; ?>
      </div>
      <div class="col-md-10 offset-md-1 bg-deraz-dark black">
        <br>
      </div>

    </div>
  </div>

  <!-- </div> -->
</section>

<section class="cards bg-deraz">
  <!-- Visit https://nicolaskadis.info/projects/pure-css-flip-cards-using-bootstrap-4-and-css-grid-no-js for an explanation -->
  <div class="container-fluid">
    <!-- <div class="cards-wrapper mr-lg-5 ml-lg-5"> -->
    <div class="cards-wrapper">
      <div class="row col-lg-10 offset-lg-1">
        <?php if (pll_current_language() == "ar") : ?>
          <h1 class="section-title text-white"><span>ماذا </span>نعمل ؟</h2>
          <?php else : ?>
            <h1 class="section-title text-white"><span><b>What</b></span> we do ?</h2>
            <?php endif; ?>

      </div>

      <div class="row cards-container col-lg-12 ">
        <!-- Card 1 -->
        <div class="col-lg-4 card-item">
          <div class="card-flip grow">
            <!-- Card 1 Front -->
            <div class="card front text-white">

              <div class="card-block">
                <h1>
                  <img src="<?php echo get_bloginfo('template_directory'); ?>/images/community.svg" class="img-fluid" alt=""></h1>
                <h1 class="text-white"><b><?= (pll_current_language() == "ar") ? "مجتمع" : "Community"; ?></b></h1>
              </div>
            </div>
            <!-- End Card 1 Front -->

            <!-- Card 1 Back -->
            <div class="card back community">
              <div class="card-block">
                <h1 class="card-title"><?= (pll_current_language() == "ar") ? "مجتمع" : "Community"; ?></h1>
                <!-- <h6 class="card-subtitle mb-2 text-muted">Back Card subtitle</h6> -->
                <?php if (pll_current_language() == "ar") : ?>
                  <p class="card-text">في دراز يجمتع الكثير من رواد الأعمال الناجحين، المعماريين، المصممين والأفراد المستقلين من ذوي الموهبة. فالعمل في محيط مشابه قد يكسبك الكثير من الأصدقاء والمعارف من مختلف التخصصات، أو قد يمكنك من مقابلة شريك العمل المستقبلي. نأمل من خلال تكوين هذا المجتمع إلى التغيير والتأثير الإيجابي على المدى الطويل</p>
                <?php else : ?>
                  <p class="card-text">
                    <?= substr("We're a community based. Bringing together the talents, resources, and skills of people in order to increase your collective power and work for social change. Our space has some of the best architects, designers, artists, content creators, developers and more. while working here, you will meet people from diverse fields, make new friends and maybe find your next business partner.", 0, 382); ?>...
                  </p>
                <?php endif; ?>

                <a href="<?= get_page_url("community"); ?>" class="card-link"><i class="fas fa-angle-right"></i></a>
              </div>
            </div>
            <!-- End Card 1 Back -->
          </div>
        </div>
        <!-- End Card 1 -->

        <!-- Card 2 -->
        <div class="col-lg-4 card-item">
          <div class="card-flip grow">
            <!-- Card 1 Front -->
            <div class="card front text-white">

              <div class="card-block">
                <h1>
                  <img src="<?php echo get_bloginfo('template_directory'); ?>/images/events.svg" class="img-fluid" alt=""></h1>
                <h1 class="text-white"><b><?= (pll_current_language() == "ar") ? "أحداث" : "Events"; ?></b></h1>
              </div>
            </div>
            <!-- End Card 1 Front -->

            <!-- Card 1 Back -->
            <div class="card back events">
              <div class="card-block">
                <h1 class="card-title"><?= (pll_current_language() == "ar") ? "أحداث" : "Events"; ?></h1>
                <!-- <h6 class="card-subtitle mb-2 text-muted">Back Card subtitle</h6> -->
                <?php if (pll_current_language() == "ar") : ?>
                  <p class="catd-text">نأمل من خلال تنظيم أحداث أسبوعية متنوعة في دراز إلى خلق بيئة ومجتمع إبداعي محفز، حيث ينظم كل سبت حدث بتقديم من كفاءات شبابية محلية شغوفة بعملها وموهوبة في عملها. نهدف من خلال تنظيم هذه الأحداث إلى نشر المعرفة، الوعي والثقافة.</p>
                <?php else : ?>
                  <p class="card-text">
                    <?= substr("A diverse cultural and networking events series form the community to the community every Saturday is hosted at our space, we collaborate with local expertise and young talented individuals, to lead these events and give back by sharing knowledge to spread the art and design culture and create a community of creative people, help them network, share information and build healthy relationships among the various groups in the community by organizing events regularly", 0, 382); ?>...
                  </p>
                <?php endif; ?>
                <a href="<?= get_page_url("events"); ?>" class="card-link"><i class="fas fa-angle-right"></i></a>

              </div>
            </div>
            <!-- End Card 1 Back -->
          </div>
        </div>
        <!-- End Card 2 -->

        <!-- Card 3 -->
        <div class="col-lg-4 card-item">
          <div class="card-flip grow">
            <!-- Card 1 Front -->
            <div class="card front text-white">

              <div class="card-block">
                <h1>
                  <img src="<?php echo get_bloginfo('template_directory'); ?>/images/programes.svg" class="img-fluid" alt=""></h1>
                <h1 class="text-white"><b><?= (pll_current_language() == "ar") ? "برامج" : "Programmes"; ?></b></h1>
              </div>
            </div>
            <!-- End Card 1 Front -->

            <!-- Card 1 Back -->
            <div class="card back programs">
              <div class="card-block">
                <h1 class="card-title"><?= (pll_current_language() == "ar") ? "برامج" : "Programmes"; ?></h1>
                <!-- <h6 class="card-subtitle mb-2 text-muted">Back Card subtitle</h6> -->
                <?php if (pll_current_language() == "ar") : ?>
                  <p class="card-text">تهدف مشاريعنا إلى تسريع ثقافة ريادة الأعمال ضمن فئة الشباب في ليبيا من خلال تبني مفهوم التفكير الإبداعي في العمل وحل المشاكل المختلفة، ويتم هذا من إدماج الشباب في سلسلة تدريبات معينة وتوجيههم من قبل إختصاصيين. أخيراً نأمل من خلال مشاريعنا هذه إلى تشجيع الشباب على خلق ريادة أعمال محلية لصالح الإقتصاد المحلي</p>
                <?php else : ?>
                  <p class="card-text"><?= substr("Our programme seeks to foster the culture of entrepreneurship among youth in Libya. Through provision of targeted technical training, coaching, and competitions. Deraz's programme always aims to prevent economic migration and encourages youth to establish domestic enterprises for all of our members.", 0, 382); ?>...</p>
                <?php endif; ?>
                <a href="<?= get_page_url("programmes"); ?>" class="card-link"><i class="fas fa-angle-right"></i></a>
                <!-- <a href="#" class="card-link">Another link</a> -->
              </div>
            </div>
            <!-- End Card 1 Back -->
          </div>
        </div>
        <!-- End Card 3 -->

      </div>
    </div>
    <div class="clearfix"></div>
    <div class="how ">
      <div class="row">
        <?php if (pll_current_language() == "ar") : ?>
          <h1 class="section-title text-white bg-deraz-dark"><span><b>الاشتراكات</b></span></h2>
          <?php else : ?>
            <h1 class="section-title text-white bg-deraz-dark"><span><b>How</b></span> it works ?</h2>
            <?php endif; ?>
      </div>
      <div class="row spaces">
        <div class="col-lg-4 space">
          <h1 class="space-img"> <span><img src="<?php echo get_bloginfo('template_directory'); ?>/images/space.svg" class="" alt=""></span></h1>
          <h1 class="section-title co-working"><b><?= __("Co-Working-space"); ?></b></h1>
          <hr>
          <h1 class="section-title"><?= __("Memberships"); ?></h1>
        </div>
        <div class="col-lg-8 plans bg-white">
          <?php

          $plans_req  = wp_remote_get(get_api_url("plans"));
          if (is_wp_error($plans_req)) {
            $plans = [];
          } else {
            $plans = wp_remote_retrieve_body($plans_req);
            $plans = json_decode($plans_req["body"])->data;
          }
          // $plans_req  = wp_remote_get(get_api_url("plans"));


          $plans_count = count($plans);

          if ($plans_count) :
            echo "<style>";

            echo ".plan-icon {";
            echo "fill: black;";
            echo "  display: inline-block;";
            echo "  width: auto;";
            echo "  height: 4rem;";
            echo "  background-size: cover;";
            echo "  background-repeat: no-repeat;";
            // echo "   filter: invert(100%);";
            echo "}";
            foreach ($plans as $plan) :
              echo ".plan-" . $plan->slug . " {";
              echo "background-image: url($plan->icon);";
              echo "}";
            endforeach;
            echo "</style>";

            $col = ($plans_count > 4) ? 4 : 6;

            // print_r($plans);
            $i = 0;
            foreach ($plans as $plan) :
              if (($i % 2 == 0 && $col == 6) || ($i % 3 == 0 && $col == 4)) {
                echo "<div class='row'>";
              }
              ?>
              <div class="col-lg-<?= $col ?> col-md-6 plan">
                <!-- <img src="<?php echo get_bloginfo('template_directory'); ?>/images/one-day.svg" alt="icon"> -->
                <!-- <img src="<?= $plan->icon; ?>" alt="icon"> -->
                <img class="plan-icon plan-<?= $plan->slug; ?>" src="<?= $plan->icon; ?>">

                <h1 class="plan-title mt-4"><?= $plan->{get_attr_in_lang("name")}; ?></h1>
              </div>
          <?php
              $i++;
              if (($i % 2 == 0 && $col == 6) || ($i % 3 == 0 && $col == 4)) {
                echo "</div>";
              }
            endforeach;
          endif;
          ?>
          <div class="row float-right col-12">
            <div class="clearfix"></div>

            <a href="<?php echo get_page_url("spaces"); ?>"><button class="btn btn-outline-success btn-flat float-right"><?= __("learn-more") ?> ></button></a>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>

<section class="derazyon">
  <div class="skew-top"></div>
  <div class="container-fluid bg-deraz">
    <div class="container">
      <h1 class="section-title text-white"><?= __("Derazyons"); ?></h1>

      <div id="carousel">
        <div class="btn-bar">
          <div id="buttons">
            <a id="prev" href="#">
              <</a> <a id="next" href="#">>
            </a>
          </div>
        </div>
        <div id="slides">
          <ul>
            <?php
            $qoutes = new WP_Query([
              "post_type" => "review",
              "posts_per_page" => 3
            ]);

            if ($qoutes->have_posts()) :
              while ($qoutes->have_posts()) : $qoutes->the_post();
                ?>
                <li class="slide">
                  <div class="quoteContainer">
                    <p class="quote-phrase"><span class="quote-marks">"</span><?= get_the_content(); ?><span class="quote-marks">"</span></p>
                  </div>
                  <div class="authorContainer">
                    <p class="quote-author"><?= the_field("author"); ?></p>
                  </div>
                </li>
            <?php
              endwhile;

            endif;
            ?>
          </ul>
        </div>
      </div>
    </div>
</section>

<section class="choose bg-deraz-dark">
  <div class="container-fluid text-white">
    <div class="row mb-5 col-lg-10 offset-lg-1">
      <h1 class="section-title text-white"><?= __("why-us"); ?></h1>
    </div>
    <div class="row text-center col-lg-10 offset-lg-1">
      <div class="col-lg-4 col-md-6 item">
        <h1><img src="<?php echo get_bloginfo('template_directory'); ?>/images/heart.svg" alt=""></h1>
        <!-- <h1><span class="icon-community"></span></h1> -->
        <h2><?= __("Community-at-heart"); ?></h2>
      </div>
      <div class="col-lg-4 col-md-6 item">
        <h1><img src="<?php echo get_bloginfo('template_directory'); ?>/images/team.svg" alt=""></h1>
        <!-- <h1><span class="icon-team"></span></h1> -->
        <!-- <h1><i class="fas fa-tags"></i></h1> -->
        <h2><?= __("Friendly-team-&-environment"); ?></h2>
      </div>
      <div class="col-lg-4 col-md-6 item">
        <h1><img src="<?php echo get_bloginfo('template_directory'); ?>/images/commitment.svg" alt=""></h1>
        <!-- <h1><span class="icon-commitment"></span></h1> -->
        <h2><?= __("No-commitment"); ?></h2>
      </div>
      <div class="col-lg-4 col-md-6 item">
        <h1><img src="<?php echo get_bloginfo('template_directory'); ?>/images/boost.svg" alt=""></h1>
        <!-- <h1><span class="icon-boost"></span></h1> -->
        <h2><?= __("Productivity-boost"); ?></h2>
      </div>
      <div class="col-lg-4 col-md-6 item">
        <h1><img src="<?php echo get_bloginfo('template_directory'); ?>/images/affordable.svg" alt=""></h1>
        <!-- <h1><span class="icon-affordable"></span></h1> -->
        <h2><?= __("Afforable"); ?></h2>
      </div>
      <div class="col-lg-4 col-md-6 item">
        <h1><img src="<?php echo get_bloginfo('template_directory'); ?>/images/flixible.svg" alt=""></h1>
        <!-- <h1><span class="icon-flixible"></span></h1> -->
        <h2><?= __("Flexible"); ?></h2>
      </div>
    </div>
  </div>
</section>

<section class="blog bg-deraz-dark">

  <div class="container">
    <?php
    $args = [
      "posts_per_page" => 3,
      // "post_type" => "spotlight"
    ];
    $blog = new WP_Query($args);

    if ($blog->have_posts()) :
      while ($blog->have_posts()) : $blog->the_post();
        ?>
        <div class="post text-justify">
          <div class="author align-items-end">
            <?php if (!empty(get_field("author_image"))) : ?>
              <img src="<?= get_field("author_image") ?>" class="img-fluid">
            <?php else : ?>
              <?php the_post_thumbnail("home_80"); ?>
              <!-- <img src="<?= get_bloginfo("template_directory"); ?>/images/author1.png" class="img-fluid" style="opacity:0"> -->
            <?php endif; ?>
            <h4><?= get_field("author"); ?> <span></span> <?php echo get_the_date(); ?></h4>
            <hr>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <a href="<?php the_permalink(); ?>">
              <h3 class="title"><?php the_title(); ?></h3>
            </a>
            <?php the_excerpt(); ?>
          </div>
        </div>
    <?php
      endwhile;


    else :
      echo "NO POSTS";
    endif;
    ?>
    <!-- <a class="btn btn-white btn-flat bg-white text-black float-right" href="<?= get_home_url(); ?>"><?= __("go-to-blog") ?></a> -->
    <div class="container text-right float-right" style="z-index:10;">
      <div class="row">
        <button class="btn-white btn-flat float-right"><a href="<?php echo get_permalink(get_option('page_for_posts')); ?>"><?= __("go-to-blog") ?></a></button>
      </div>
    </div>
  </div>
</section>


<?php get_footer(); ?>