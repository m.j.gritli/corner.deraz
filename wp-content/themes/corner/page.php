<?php get_header(); ?>
<div class="bg-deraz-dark top-about-section">
    <h2 class="bg-deraz text-center"><?php the_title(); ?></h2>
</div>
<div class="container-fluid px-0">

    <?php
    if (have_posts()) :

        while (have_posts()) : the_post();
            echo the_content();
        endwhile;

    endif;
    ?>
</div>


<?php get_footer(); ?>