 <?php
    /*
Template Name: Reviews
*/
    ?>

 <?php
    $all_categories = get_categories();

    $review_args = array(
        'post_type' => 'review',
        'posts_per_page' => 10,
        'paged' => $paged,
    );

    $review_posts = new WP_Query($review_args, ARRAY_A);

    ?>
 <?php get_header(); ?>
 <div class="bg-deraz-dark top-about-section">
     <h2 class="bg-deraz text-center"><?php the_title(); ?></h2>
 </div>
 <div class="container-fluid mt-5 py-5 ">
     <div class="row">
         <div class="push-top bg-deraz-dark text-center text-white">
             <h1 class="text-center d-inline-block title-style-deraz text-white"><b>What</b> our members say about Us</h1>
             <p class="text-center">Below you'll find honest reviews from people who used and hopefully loved our workspace, want to be heard? </p><a class="d-inline-block flat-button-deraz" href="<?= get_page_url("contact-us") ?>">Contact Us</a>
         </div>
     </div>
 </div>
 <div class="py-5">
     <div class="container">
         <div class="row">

             <!-- <div class="col-md-2 mx-auto p-3"> <img class="img-fluid d-block rounded-circle" src="https://static.pingendo.com/img-placeholder-2.svg"> </div> -->
             <?php
                if ($review_posts->have_posts()) :
                    while ($review_posts->have_posts()) : $review_posts->the_post();
                        ?>
             <div class="p-3 col-lg-6 col-md-12">
                 <div class="blockquote mb-0">
                     <h2 class="review-title"><?= the_title(); ?></h2> <?php print_stars(get_field('stars')) ?>
                     <p class="border-left-deraz-qoute text-justify">"<?= get_the_excerpt() ?>"</p>
                     <div class="blockquote-footer">
                         <img class="img-fluid d-inline-block rounded-circle mx-2" src="<?= get_field("author_image") ?>"><b><?= get_field('author') ?></b></div>
                 </div>
             </div>
             <?php
                    endwhile;
                    ?>
             <div class="clearfix">
                 <div class="container">
                     <?php
                            echo bootstrap_pagination();
                            endif;
                            ?>
                 </div>
             </div>
             

         </div>
     </div>
 </div>

 <?php get_footer(); ?>