<?php get_header(); ?>
    <!-- Page Body  -->
    <div class="spaces-header" style="background-image: url(./images/header-people.jpg)">
      <!-- <img src="./images/header-people.jpg" alt=""> -->
      <h1>The sociable way to work</h1>
      <ul class="nav nav-tabs tabs-header" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="co-working-tab" data-toggle="tab" href="#co-working" role="tab">co-working spaces</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="training-tab" data-toggle="tab" href="#training" role="tab">training & meeting room</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab">event space</a>
          </li>
        </ul>
        <div class="tab-content tabs-content" id="myTabContent">
          <div class="tab-pane fade show active" id="co-working" role="tabpanel" >
            <div class="row">
                <div class="col-lg-3 pt-5 pl-4 bg-deraz text-white">
                    <!-- <img src="./images/one-day.svg"> -->
                    <h3 class="text-white mt-3"> Day Pass</h3>          
                    <p class="thick-border-bottom"></p>
                    <p>Description</p>
                    <ul>
                      <li>
                        <p>1</p>
                      </li>
                      <li>
                        <p>2</p>
                      </li>
                      <li>
                        <p>4</p>
                      </li>
                      <li>
                        <p>3</p>
                      </li>
                    </ul>
                </div>
                <div class="col-lg-3 pt-5 pl-4 bg-deraz text-white">
                    <!-- <img src="./images/one-day.svg"> -->
                    <h3 class="text-white mt-3"> Day Pass</h3>          
                    <p class="thick-border-bottom"></p>
                    <p>Description</p>
                    <ul>
                      <li>
                        <p>1</p>
                      </li>
                      <li>
                        <p>2</p>
                      </li>
                      <li>
                        <p>4</p>
                      </li>
                      <li>
                        <p>3</p>
                      </li>
                    </ul>
                </div>
                <div class="col-lg-3 pt-5 pl-4 bg-deraz text-white">
                    <!-- <img src="./images/one-day.svg"> -->
                    <h3 class="text-white mt-3"> Day Pass</h3>          
                    <p class="thick-border-bottom"></p>
                    <p>Description</p>
                    <ul>
                      <li>
                        <p>1</p>
                      </li>
                      <li>
                        <p>2</p>
                      </li>
                      <li>
                        <p>4</p>
                      </li>
                      <li>
                        <p>3</p>
                      </li>
                    </ul>
                </div>
                <div class="col-lg-3 pt-5 pl-4 bg-deraz text-white">
                    <!-- <img src="./images/one-day.svg"> -->
                    <h3 class="text-white mt-3"> Day Pass</h3>          
                    <p class="thick-border-bottom"></p>
                    <p>Description</p>
                    <ul>
                      <li>
                        <p>1</p>
                      </li>
                      <li>
                        <p>2</p>
                      </li>
                      <li>
                        <p>4</p>
                      </li>
                      <li>
                        <p>3</p>
                      </li>
                    </ul>
                </div>
            </div>
          </div>
          <div class="tab-pane fade" id="training" role="tabpanel" >div2</div>
          <div class="tab-pane fade" id="contact" role="tabpanel" >div3</div>
        </div>
    </div>

<?php get_footer(); ?>