<?php

// Register Custom Navigation Walker bootstra 3
// require_once('wp_bootstrap_navwalker.php');
// Register Custom Navigation Walker bootstra 4
if (!file_exists(get_template_directory() . '/class-wp-bootstrap-navwalker.php')) {
    // file does not exist... return an error.
    return new WP_Error('class-wp-bootstrap-navwalker-missing', __('It appears the class-wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker'));
} else {
    // file exists... require it.
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}

// register_nav_menus( 
//   array('main' => __( 'main', 'MobiMal WP Theme' )) );

add_theme_support('menus');
register_nav_menus(array('main' => __('home menu')));
// register_nav_menus( array( 'pages' => __('pages menu') ) );

function dd($arg) {
    ob_start();
    echo "<pre>";
        var_dump($arg);
    echo "</pre>";
    // die();
}

 function get_attr_in_lang($param)
{
    $lang = pll_current_language();
    
    return $param."_".$lang;
}

function print_categories_as_tags($categories ,$args = ''){
    $classes= 'badge bg-soft mx-1 '.$args;
    ob_start();
    foreach($categories as $cat){
        echo '<a class="'.$classes.'">'.$cat->name.'</a>';
    }
}
function get_api_url($params) {
    return "http://my.corner.deraz.ly/api/".$params;
}
function get_page_url($param){
        wp_reset_query();
        if(is_string($param)) {
            $q = new WP_Query(["pagename" => $param]);
        }elseif(is_numeric(($param))) {
            $q = new WP_Query(["page_id" => $param]);
        }
        
        $page_url = "";
        if ($q->have_posts()) :
            while ($q->have_posts()) : $q->the_post();
            $page_url = get_the_permalink();
            endwhile;
        else :
            $page_url = "#";
        endif;
    return $page_url;    
}

//wordpress pagination paginate_links
function sa_get_bootstrap_paginate_links()
{
    ob_start();
    ?>
    <div class="pages clearfix">
        <?php
        global $wp_query;
        $current = max(1, absint(get_query_var('paged')));
        $pagination = paginate_links(array(
            'base' => str_replace(PHP_INT_MAX, '%#%', esc_url(get_pagenum_link(PHP_INT_MAX))),
            'format' => '?paged=%#%',
            'current' => $current,
            'total' => $wp_query->max_num_pages,
            'type' => 'array',
            'prev_text' => '&laquo;',
            'next_text' => '&raquo;',
        )); ?>
        <?php if (!empty($pagination)) : ?>
            <ul class="pagination">
                <?php foreach ($pagination as $key => $page_link) : ?>
                    <li class="paginated_link<?php if (strpos($page_link, 'current') !== false) {
                                                    echo ' active';
                                                } ?>"><?php echo $page_link ?></li>
                <?php endforeach ?>
            </ul>
        <?php endif ?>
    </div>
    <?php
    $links = ob_get_clean();
    return apply_filters('sa_bootstap_paginate_links', $links);
}
function sa_bootstrap_paginate_links()
{
    // echo sa_get_bootstrap_paginate_links();
}


/**
 * adding my style to wp-admin for live preview effects of my classes
 * 
 */
// Update CSS within in Admin
// function admin_style() {
//   wp_enqueue_style('admin-styles', get_template_directory_uri().'/style.css');
// }
// add_action('admin_enqueue_scripts', 'admin_style');

// add_filter( 'widget_text', 'shortcode_unautop');
// add_filter( 'widget_text', 'do_shortcode');

// check if  string ends with specific sub-string
function endsWith($currentString, $target)
{
    $length = strlen($target);
    if ($length == 0) {
        return true;
    }
 
    return (substr($currentString, -$length) === $target);
}

function print_stars($rating){
    if(endsWith($rating,".5")){//rating ends with .5
        for($i=1;$i<=$rating;$i++){
            echo '<div class="star"></div>';
        }
        echo '<div class="half-star"></div>';
    }else{
        for($i=1;$i<=$rating;$i++){
            echo '<div class="star"></div>';
        }
        
    }
}

// function max_title_length( $title ) {
// $max = 49;
// if( strlen( $title ) > $max ) {
// return substr( $title, 0, $max ). " &hellip;";
// } else {
// return $title;
// }
// }

// add_filter( 'the_title', 'max_title_length');


if (function_exists('register_sidebar'))
    register_sidebars(2, array(
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div><!--/widget-->',
        'before_title' => '<h2 class="hl">',
        'after_title' => '</h2>',
    ));

function show_avatar($comment, $size)
{
    $email = strtolower(trim($comment->comment_author_email));
    $rating = "G"; // [G | PG | R | X]

    if (function_exists('get_avatar')) {
        echo get_avatar($email, $size);
    } else {

        $grav_url = "http://www.gravatar.com/avatar.php?gravatar_id=
         " . md5($emaill) . "&size=" . $size . "&rating=" . $rating;
        echo "<img src='$grav_url'/>";
    }
}

if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    add_image_size('news-thumb', 350, 350, array('center', 'center')); // (cropped)

    add_image_size('square_200', 200, 200, array('center', 'center')); // (cropped)
    add_image_size('home_80', 70, 70, array('center', 'center')); // (cropped)


    // 50 pixels wide by 50 pixels tall, crop from the center
}



// add item at the end of nav
// function modifiy_nav_menu_items($items, $args) {

// $myargs = array('dropdown' => 1, "show_flags" => 1, "echo" => 1);
// return $items.= "<li class='pll-parent-menu-item menu-item menu-item-type-custom menu-item-object-custom current-menu-parent menu-item-has-children dropdown'>".pll_the_languages($myargs)."</li>";
// }
// add_action( 'wp_nav_menu_items', 'modifiy_nav_menu_items');



// function register_featured_content_support() {
// 	// Adds featured content support to the custom post type 'my-post-type-slug'
// 	add_post_type_support('my-post-type-slug', 'featured-content');

// 	// Removes featured content support from pages
// 	remove_post_type_support('page', 'featured-content');
// }
// add_filter('after_setup_theme', 'register_featured_content_support');

function bg_recent_comments($no_comments = 5, $comment_len = 80, $avatar_size = 48)
{
    $comments_query = new WP_Comment_Query();
    $comments = $comments_query->query(array('number' => $no_comments));
    $comm = '';
    if ($comments) : foreach ($comments as $comment) :
            $comm .= '<li><a class="author" href="' . get_permalink($comment->comment_post_ID) . '#comment-' . $comment->comment_ID . '">';
            $comm .= get_avatar($comment->comment_author_email, $avatar_size);
            $comm .= get_comment_author($comment->comment_ID) . ':</a> ';
            $comm .= '<p>' . strip_tags(substr(apply_filters('get_comment_text', $comment->comment_content), 0, $comment_len)) . '...</p></li>';
        endforeach;
    else :
        $comm .= 'No comments.';
    endif;
    echo $comm;
}


// limit the numbers of words in content or excerpt
// if (!function_exists( 'my_excerpt_length' )) {
// 	function my_excerpt_length($length) {
// 		return 28; // Or whatever you want the length to be.
// 	}
// 	add_filter('excerpt_length', 'my_excerpt_length');
// }

// limit the numbers of words in content or excerpt
// if (!function_exists( 'my_excerpt_length' )) {
//  function my_excerpt_length($length) {
//    return 15; // Or whatever you want the length to be.
//  }
//  add_filter('excerpt_length', 'my_excerpt_length');
// }

//get short excerpt with your defined
// function get_short_excerpt($excerpt = '', $strcnt = 100)
// {
//     $x = substr($excerpt, 0, $strcnt);
//     // if(length($x) > $strcnt) $x.="...";
//     if (strlen($excerpt) > $strcnt) {
//         return $x . "...";
//     } else {
//         return $excerpt;
//     }
// }


/**
 * @param WP_Query|null $wp_query
 * @param bool $echo
 *
 * @return string
 * Accepts a WP_Query instance to build pagination (for custom wp_query()), 
 * or nothing to use the current global $wp_query (eg: taxonomy term page)
 * - Tested on WP 4.9.5
 * - Tested with Bootstrap 4.1
 * - Tested on Sage 9
 *
 * USAGE:
 *     <?php echo bootstrap_pagination(); ?> //uses global $wp_query
 * or with custom WP_Query():
 *     <?php 
 *      $query = new \WP_Query($args);
 *       ... while(have_posts()), $query->posts stuff ...
 *       echo bootstrap_pagination($query);
 *     ?>
 */
function bootstrap_pagination( \WP_Query $wp_query = null, $echo = true ) {

	if ( null === $wp_query ) {
		global $wp_query;
	}

	$pages = paginate_links( [
			'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
			'format'       => '?paged=%#%',
			'current'      => max( 1, get_query_var( 'paged' ) ),
			'total'        => $wp_query->max_num_pages,
			'type'         => 'array',
			'show_all'     => false,
			'end_size'     => 3,
			'mid_size'     => 1,
			'prev_next'    => true,
			'prev_text'    => __( '« Prev' ),
			'next_text'    => __( 'Next »' ),
			'add_args'     => false,
			'add_fragment' => ''
		]
	);

	if ( is_array( $pages ) ) {
		//$paged = ( get_query_var( 'paged' ) == 0 ) ? 1 : get_query_var( 'paged' );

		$pagination = '<div class="pagination"><ul class="pagination">';

		foreach ($pages as $page) {
                        $pagination .= '<li class="page-item' . (strpos($page, 'current') !== false ? ' active' : '') . '"> ' . str_replace('page-numbers', 'page-link', $page) . '</li>';
                }

		$pagination .= '</ul></div>';

		if ( $echo ) {
			echo $pagination;
		} else {
			return $pagination;
		}
	}

	return null;
}

function get_plans_cards(){
    ob_start();
    ?>
      <!-- row 1 -->
  <div style="background-image: url(<?php echo get_bloginfo('template_directory'); ?>/images/arrows_right.svg); background-size: cover; background-attachment: fixed;">
  <?php if (pll_current_language() == "ar") : ?>
  <h2 class="section-header display-4 bg-deraz-dark ">مساحة عمل مشتركة</h2>
          <?php else : ?>
          <h2 class="section-header display-4 bg-deraz-dark ">Co-Working <b>Space</b></h2>
          <?php endif; ?>

    <div class="row space-plans">
      <?php

      $plans_req  = wp_remote_get(get_api_url("plans"));
      $plans = json_decode($plans_req["body"])->data;
      $plans_count = count($plans);
      // print_r($plans);
      if ($plans_count) :
        echo "<style>";

        echo ".plan-icon {";
        echo "fill: black;";
        echo "  display: inline-block;";
        echo "  width: auto;";
        echo "  height: 3.5rem;";
        echo "  background-size: cover;";
        echo "  background-repeat: no-repeat;";
        echo "   filter: invert(100%);";
        echo "}";
        foreach ($plans as $plan) :
          echo ".plan-" . $plan->slug . " {";
          // echo "background-image: url($plan->icon);";
          echo "}";
        endforeach;
        echo "</style>";
      endif;
      foreach ($plans as $plan) :
        ?>
        <!-- plan col  -->
        <div class="col-lg-3 col-md-6 col-sm-12 m-0 p-2 plan">
          <div class="bg-deraz plan-block plan-block-grid">
            
            <?php $name = "name_" . pll_current_language(); ?>
            <img class="plan-icon plan-<?= $plan->slug; ?>" src="<?= $plan->icon ?>" alt="">
            <h3 class="text-white">
              <?= ucwords($plan->$name); ?>
            </h3>
            <hr class="thick-border-bottom ">
            <ul>
              <?php foreach ($plan->services as $service) : ?>
                <li>
                  <!-- <i class="fa fa-<?= $service->icon; ?>"></i> -->
                  <i class="fas fa-<?= $service->icon; ?>"></i>
                  <!-- <img src="<?php echo get_bloginfo('template_directory'); ?>/images/one-day.svg" alt="" height="20"> -->
                  <p>
                    <?= ucwords($service->$name); ?>
                  </p>
                </li>
              <?php endforeach; ?>
              <li><i class="fas fa-tags"></i> <?= __("prices"); ?>
                <ul style="list-style: circle; margin-left:2rem;">
                  <?php foreach ($plan->prices as $price) : ?>
                    <li>
                      <b><?= ucwords($price->$name); ?>: <?= $price->price; ?> LYD</b>
                    </li>
                  <?php endforeach; ?>
                </ul>
              </li>
            </ul>
          </div>
          <div class="bg-deraz plan-block">
            <h5><a href="htts://my.corner.deraz.ly/login"><?= __("join-us") ?></a></h5>
          </div>
        </div>
      <?php endforeach; ?>

    </div>


  </div>
  <?php
  $output_string = ob_get_contents();
  ob_end_clean();
  return $output_string;
} 
add_shortcode('print_plans_cards', 'get_plans_cards');


?>