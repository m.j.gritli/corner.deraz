<footer class="myfooter">
  <div class="row">
    <div class="block col-lg-6 bg-deraz">
      <div class="">
        <h2 class="border-white-bottom-fit">
          <?= __('get-in-touch') ?>
        </h2>
        <p class="get-in-touch-text">
          <?php $lang = pll_current_language();
          if ($lang == "en") : ?>
            Don't hesitate to contact us for any information, questions,or others <br>
            Our team is always around to help!
          <?php else : ?>

            لا تتردد بالإستفسار عن أي سؤال، معلومة أو أي ماتريد!
            <br>
            فريقنا سيكون متواجد دائماً لمساعدتك
          <?php endif ?>
        </p>

        <h2 class="border-white-bottom-fit">
          <?= __('visit-us') ?>
        </h2>
      </div>
      <div class="footer-map">
        <!-- <a href="https://goo.gl/maps/ybeXTZvBzvRm94Vs9"><img src="<?php echo get_bloginfo('template_directory'); ?>/images/map.png" alt=""></a> -->


        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3350.395425876415!2d13.219390496332386!3d32.88771196208906!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13a892f56601cc91%3A0x751c6c589055944f!2sDeraz%20Corner!5e0!3m2!1sen!2sly!4v1567090416927!5m2!1sen!2sly" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>


      </div>
    </div>
    <div class="block col-lg-6 bg-white">
      <h2>
        <?= __('contact-us') ?>
      </h2>
      <!-- <form action="" class="footer-form">
        <input type="text" class="form-control" placeholder="Name">
        <input type="text" class="form-control" placeholder="Email">
        <textarea name="message" class="form-control" id="" rows="7" placeholder="Your Message"></textarea>
        <button class="btn btn-outline-success btn-flat">Send</button>
      </form> -->
      <!--  echo do_shortcode('[contact-form-7 id="119" title="Contact form Home_en"]'); -->
      <?php
      if ($lang == "en") :
        echo do_shortcode('[contact-form-7 id="103" title="Contact form Footer"]');
      else :
        echo do_shortcode('[contact-form-7 id="303" title="Contact form Footer-ar"]');
      endif; ?>
    </div>
  </div>
  <!-- desktop lower footer  -->
  <div class="row block bg-deraz-dark">
    <div class="col-lg-6">

    </div>
    <div class="col-lg-6 footer-text">
      <div class="row pt-5 footer-row">
        <div class="col-md-4">
          <a href="https://goo.gl/maps/ybeXTZvBzvRm94Vs9" class="social-links">
            <h3 class="text-white">Location</h3>
            <p>
              <i class="fas fa-map-marked-alt fa-2x"></i>
              Alnooflien,Tripoli
            </p>
          </a>
        </div>
        <div class="col-md-4">
          <a href="mailto: corner@deraz.ly" class="social-links">
            <h3 class="text-white">Information</h3>
            <p>
              <i class="far fa-envelope fa-2x"></i>
              <span style="font-size:1.25rem; vertical-align: super;">
                corner@deraz.ly
              </span>
            </p>
          </a>
        </div>
        <div class="col-md-4">
          <a href="https://www.coworker.com/libya/tripoli/deraz-corner" class="widget-link img-fluid">
            <img src="https://coworker.imgix.net/add-images/img_tedx-widget.png" class="widget-image" alt="widget Pic" />
          </a>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row pt-5 footer-row">
        <div class="col-lg-4 col-md-4">
          <h3 class="text-white">Social Media</h3>
          <p>
            <a href="https://www.youtube.com/channel/UCZJNStC_muShmHMLcMyJYfg" class="social-links">
              <i class="fab fa-youtube fa-2x"></i>
            </a>
            <a href="https://www.instagram.com/deraz.corner/" class="social-links">
              <i class="fab fa-instagram fa-2x"></i>
            </a>
            <a href="https://www.facebook.com/DERAZCorner" class="social-links">
              <i class="fab fa-facebook-f fa-2x"></i>
            </a>
            <a href="https://twitter.com/deraz.corner" class="social-links">
              <i class="fab fa-twitter fa-2x"></i>
            </a>
          </p>

        </div>
        <div class="col-lg-4 col-md-4">
          <h3 class="text-white"><?php echo __("call-us"); ?></h3>
          <p>
            <a href="tel:00218923450009" class="social-links"> <i class="fas fa-phone"></i> + 218 92 345 0009</a> <br />
            <!-- <a href="tel:00218925554433" class="social-links"> <i class="fas fa-phone"></i> + 218 92 5554433</a> -->
          </p>
        </div>
        <div class="col-lg-4 col-md-4">
          <!-- <h3 class="text-white">Phones</h3> -->
          <a href="https://www.coworker.com/libya/tripoli/deraz-corner" class="widget-link img-fluid">
            <img src="https://coworker.imgix.net/add-images/img_member_say_green01.png" class="widget-image" alt="widget Pic">
          </a>
        </div>
      </div>
    </div>

  </div>
  <div class="bg-black copyrights-block" dir="ltr">
    <span class="copyrights text-white navbar-brand">
      Copyright &copy; <?= date("Y"); ?>
      <a class="text-white navbar-brand mr-1"><b>Deraz</b>,</a>All rights reserved. Developed by
      <a href="https://www.websers.ly" class="text-white navbar-brand mr-1"><b>Websers</b></a>
    </span>
  </div>
</footer>
<?php wp_footer(); ?>

<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/popper.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/aos.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.sticky.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/stickyfill.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.easing.1.3.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.fancybox.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/main.js"></script>

<script src="<?php echo get_bloginfo('template_directory'); ?>/js/events.js"></script>

<!-- jQuery -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/blog/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/blog/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/blog/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/blog/jquery.waypoints.min.js"></script>
<!-- Flexslider -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/blog/jquery.flexslider-min.js"></script>
<!-- Owl carousel -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/blog/owl.carousel.min.js"></script>
<!-- Magnific Popup -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/blog/jquery.magnific-popup.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/blog/magnific-popup-options.js"></script>
<!-- Main -->
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/blog/main.js"></script>



<script>
  var searchOpen = false;
  var menuOpen = false;

  function openNav() {
    document.getElementById("myNav").style.width = "100%";
    menuOpen = true;
  }

  function closeNav() {
    document.getElementById("myNav").style.width = "0%";
    menuOpen = false;

  }

  $('body').on('keydown', function(event) {
    // Hide on escape.
    if (event.keyCode == 27) {
      if (menuOpen) {
        closeNav();
      }
      if (searchOpen) {
        closeSearch();
      }
    }
  });

  function openSearch() {
    document.getElementById("mySearch").style.width = "100%";
    searchOpen = true;
  }

  function closeSearch() {
    document.getElementById("mySearch").style.width = "0%";
    searchOpen = false;
  }

  $(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 100) {
      $(".site-navbar").addClass("bg-deraz-dark");


    } else {
      $(".site-navbar").removeClass("bg-deraz-dark");

    }
  });

  // add class to footer form
  $(".wpcf7-form").addClass("footer-form");
  //add classes to wow
  $(".wow").data("wow-duration", "1s");
  $(".wow").data("wow-delay", "1s");

  //add style to wow
  $(".wow").css("visibility", "visible");
  $(".wow").css("animation-duration", "1s");
  $(".wow").css(" animation-delay", "1s");

  $(document).ready(function() {

    $(".co-working").html(function() {
      var text = $(this).text().trim().split(" ");
      var first = text.shift();
      return (text.length > 0 ? "<span class='first-word'>" + first + "</span> " : first) + text.join("</br>");
    });
    $(document).ready(function() {

      //video heigh to current div

      $(".fb-video").data("width", $(".fb-video").parent().width());


      //rotation speed and timer
      var speed = 5000;

      var run = setInterval(rotate, speed);
      var slides = $('.slide');
      var container = $('#slides ul');
      var elm = container.find(':first-child').prop("tagName");
      var item_width = container.width();
      var previous = 'prev'; //id of previous button
      var next = 'next'; //id of next button
      slides.width(item_width); //set the slides to the correct pixel width
      container.parent().width(item_width);
      container.width(slides.length * item_width); //set the slides container to the correct total width
      container.find(elm + ':first').before(container.find(elm + ':last'));
      resetSlides();


      //if user clicked on prev button

      $('#buttons a').click(function(e) {
        //slide the item

        if (container.is(':animated')) {
          return false;
        }
        if (e.target.id == previous) {
          container.stop().animate({
            'left': 0
          }, 1500, function() {
            container.find(elm + ':first').before(container.find(elm + ':last'));
            resetSlides();
          });
        }

        if (e.target.id == next) {
          container.stop().animate({
            'left': item_width * -2
          }, 1500, function() {
            container.find(elm + ':last').after(container.find(elm + ':first'));
            resetSlides();
          });
        }

        //cancel the link behavior            
        return false;

      });

      //if mouse hover, pause the auto rotation, otherwise rotate it    
      container.parent().mouseenter(function() {
        clearInterval(run);
      }).mouseleave(function() {
        run = setInterval(rotate, speed);
      });


      function resetSlides() {
        //and adjust the container so current is in the frame
        container.css({
          'left': -1 * item_width
        });
      }

    });
    //a simple function to click next link
    //a timer will call this function, and the rotation will begin

    function rotate() {
      $('#next').click();
    }
  });
  <?php
  global $post;
  global $wp;
  $current_slug = add_query_arg(array(), $wp->request);
  // echo $current_slug;
  $post_slug = $post->post_name;
  if ((is_singular(array('spotlight_pt', 'post'))) || (is_singular(array('opportunity_pt', 'post'))) && $current_slug != 'en/contact-us') :
    ?>
    var emailph = "<?= __("your-email") ?>";
    var nameph = "<?= __("your-name") ?>";
    var textareaph = "<?= __("your-message") ?>";
    $('.comment-form input').addClass("form-control");
    $('.comment-form textarea').addClass("form-control").attr("placeholder", textareaph);
    $('.comment-form #submit').addClass("btn btn-primary").removeClass("form-control");
    $('.comment-form #email').attr("placeholder", emailph);
    $('.comment-form #author').attr("placeholder", nameph);
    $('.comment-form #url').remove();
    $('.comment-form label').remove();
    $('.comment-form .comment-notes').remove();
  <?php
  endif
  ?>
  <?php

  if ($current_slug == 'en/contact-us') :
    ?>
    $(".wpcf7-form").removeClass("footer-form");
    $(".wpcf7-form").addClass("email-form pb-0 mb-4");
    $("#email-form-div").addClass("bg-stripes-black");
    $(".wpcf7-form input").addClass("form-control");
    $(".wpcf7-form textarea").addClass("form-control");
    $(".wpcf7-form select").addClass("form-control custom-select").attr('style', 'height:50px;');
    $(".wpcf7-form .wpcf7-submit").addClass("btn btn-primary").removeClass("form-control").attr('style', 'display:flex;margin:.5rem auto;');

  <?php
  endif
  ?>
  <?php
  if ($current_slug == 'ar/contact-us-2') :
    ?>
    $(".wpcf7-form").removeClass("footer-form");
    $(".wpcf7-form").addClass("email-form pb-0 mb-4");
    $("#email-form-div").addClass("bg-stripes-black");
    $(".wpcf7-form input").addClass("form-control text-left");
    $(".wpcf7-form textarea").addClass("form-control");
    $(".wpcf7-form select").addClass("form-control custom-select").attr('style', 'height:50px;');
    $(".wpcf7-form .wpcf7-submit").addClass("btn btn-primary").removeClass("form-control").attr('style', 'display:flex;margin:.5rem auto;');

  <?php
  endif
  ?>
</script>

<script>
  let blocks = $('.plan-block-grid');
  let maxHeight = 0;
  for (var i = 0; i < blocks.length; i++) {
    if (blocks[i].clientHeight > maxHeight) {
      maxHeight = blocks[i].clientHeight;
      $('.plan-block-grid').css('min-height', maxHeight);
    }
  }
</script>
</body>

</html>