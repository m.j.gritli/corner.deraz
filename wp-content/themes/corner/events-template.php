<?php
/*
Template Name: Events
*/
?>
<?php get_header(); ?>
<!-- Page Body  -->
<div class="bg-deraz-dark top-about-section">
    <h2 class="bg-deraz text-center"><?php the_title(); ?></h2>
</div>

<!-- Events -->

<div class="events">
    <div class="container">
        <div class="row">

            <?php
            // $page = ($_GET("page") != null) ? $_GET("page"):1;
            $events_req  = wp_remote_get(get_api_url("events?page=" . $page));
            $events_reply = json_decode($events_req["body"]);

            $events =  $events_reply->data;

            if (count($events)) :
                $i = 0;
                foreach ($events as $event) : ?>
                    <!-- Event -->
                    <div class="event col-12">
                        <?php if ($i % 2 == 0) : ?>
                            <div class="row row-lg-eq-height">
                                <div class="col-lg-12  event_col">
                                    <div class="event_image_container">
                                        <div class="background_image" style="background:url('<?= $event->photo; ?>') top left no-repeat; background-size:contain;">
                                            <img src="<?= $event->photo; ?>" class="img-fluid" alt="" style="opacity:0;">
                                        </div>
                                        <div class="date_container">
                                            <?php
                                                        $date = new DateTime($event->start_date);
                                                        $time = new DateTime($event->start_time);
                                                        ?>
                                            <a href="#">
                                                <span class="date_content d-flex flex-column align-items-center justify-content-center">
                                                    <div class="date_day"><?= $date->format('d'); ?></div>
                                                    <div class="date_month"> <?= $date->format('m'); ?></div>
                                                    <div class="date_month"> <?= $date->format('Y'); ?></div>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row row-lg-eq-height">
                                <div class="col-lg-8 event_col">
                                    <div class="event_content">
                                        <h1 class="event_title border-left-deraz pl-2"><?= $event->{get_attr_in_lang("title")}; ?></h1>
                                        <!-- <div class="event_location">@ Miami Auditorium</div> -->
                                        <div class="event_text">

                                            <p><?= $event->{get_attr_in_lang("description")}; ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 event_col event_info pt-5">
                                    <div class="event_speakers">
                                        <!-- Event Speaker -->
                                        <div class="event_speaker flex-row align-items-center justify-content-start">
                                            <div>
                                                <p class="event_date">
                                                    <i class="far fa-calendar-alt"></i> <?= $date->format('d-m-Y'); ?>
                                                    <br>
                                                </p>
                                                <p class="event_time">
                                                    <i class="far fa-clock"></i> <?= $time->format('H:i'); ?>
                                                </p>
                                                <p class="event_speaker_name">
                                                    <i class="fas fa-users"></i> <?= $event->instructor; ?>
                                                </p>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="event_speaker_content">
                                                <div class="event_name">

                                                </div>
                                                <!-- <div class="event_speaker_title">Marketing Specialist</div> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="event_buttons">
                                        <!-- <div class="button event_button event_button_1"><a href="#">Buy Tickets Now!</a></div> -->
                                        <!-- <div class="button_2 event_button event_button_2"><a href="#">Read more</a></div> -->
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <!-- /Event -->
                    <?php $i++; ?>
                <?php endforeach;

                else : ?>
                <h2>NO events for now </h2>
            <?php endif; ?>


        </div>
        <?php

        // $pagiation =  json_decode($events_req["body"])->meta;
        $pagiation =  $events_reply;
        // print_r($pagiation);

        global $wp;
        $current_url = home_url($wp->request);

        if ($pagiation->last_page > 1) :
            ?>
            <div class="row">
                <div class="col">
                    <div class="pagination">
                        <ul>
                            <?php for ($i = 1; $i <= $pagiation->last_page; $i++) : ?>
                                <li class="<?= $pagiation->current_page == $i ? "active" : "" ?>"><a href="<?= $current_url . "?page=" . $i;  ?>"><?= $i ?></a></li>
                            <?php endfor; ?>


                        </ul>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>