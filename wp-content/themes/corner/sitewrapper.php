
    <div class="site-navbar-wrap">

    
      <div class="site-navbar site-navbar-target js-sticky-header">
        <div class="container-fluid">
          <div class="row align-items-center">

            <div class="col-6 col-md-5 col-sm-7 logo">
              <a href="<?= get_bloginfo("url"); ?>"><img class="site-logo img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/images/logo.svg" alt="DERAZ CORNER"></a>
              <a href="#" onclick="openSearch()"><img class="site-search img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/images/search.svg" alt="DERAZ CORNER"></a>
            </div>
            <div class="col-6 col-md-7 col-sm-5">


             
              

              <nav class="site-navigation text-right" role="navigation">
                <div class="container">
                  <!--class= 'js-menu-toggle' -->
                  <!-- <div class="d-inline-block d-lg-block ml-md-0 mr-auto py-3"><a href="#"  class="site-menu-toggle js-menu-toggle text-black"> -->
                  <div class="d-inline-block d-lg-block ml-md-0 mr-auto py-3">
                      
                     <ul class="nav"> 
                      <?php 
                        pll_the_languages();
                      ?>
                      <li>
                        <a href="#" onclick="openNav()" class="site-menu-toggle text-black">
                      <!-- <span class="icon-menu h3"></span> <span class="menu-text"></span> -->
                      <img src="<?php echo get_bloginfo('template_directory'); ?>/images/menu.svg" alt="">
                    </a>
                      </li>
                      </ul>
                    </div>

                  <ul class="site-menu main-menu js-clone-nav d-none d-lg-none">
                    <li><a href="#home-section" class="nav-link">Home</a></li>
                    <li><a href="#about-section" class="nav-link">About Us</a></li>
                    <li><a href="#what-we-do-section" class="nav-link">What We Do</a></li>
                    <li><a href="#portfolio-section" class="nav-link">Portfolio</a>
                      <ul class="drop-down collapsed">
                        <li><a href="#portfolio-section" class="nav-link">Portfolio</a>
                        <li><a href="#portfolio-section" class="nav-link">Portfolio</a>
                        <li><a href="#portfolio-section" class="nav-link">Portfolio</a>
                      </ul>
                    </li>
                    <li><a href="#contact-section" class="nav-link">Contact</a></li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- END .site-navbar-wrap -->