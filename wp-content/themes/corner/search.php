<?php
$all_categories = get_categories();
?>
<?php get_header(); ?>
<div class="bg-deraz-dark top-about-section">
	<h2 class="bg-deraz text-center"><?php the_title(); ?></h2>
</div>
<div id="colorlib-container">

<?php
	if (have_posts()) :
		while (have_posts()) : the_post();
			?>
			
			<div class="container-fluid bg-deraz-dark py-4" style="background-image: url(<?php echo get_bloginfo('template_directory'); ?>/images/Component1.svg);background-size: cover;background-repeat: no-repeat;">
				<div class="container my-5 py-4" >
					<div class="row bg-deraz-dark">
						<div class="col-lg-6">
							<a href="<?= the_permalink() ?>">
								<?= get_the_post_thumbnail($post, 'full', array('class' => 'img-fluid img-responsive')); ?>
							</a>
						</div>
						<div class="col-lg-6 text-white">
							<a href="<?= the_permalink() ?>">
								<h1 class="display-4 border-left-deraz pl-4 text-white">
									<?= the_title() ?>
								</h1>
							</a>
							<?= the_content(); ?>
						</div>
					</div>
				</div>
			</div>			
			<?php 
			endwhile;
			?>
        <div class="clearfix">
		<div class="container">
			
			<?php
            echo bootstrap_pagination($spotlight_posts);
        else:
        ?>
            NO POSTS FOUND;
        <?php
        endif;
		
		?>
		</div>
	</div>

</div>
<?php get_footer(); ?>