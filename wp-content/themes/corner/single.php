   <?php
	$all_categories = get_categories();

	$args = array(
		'posts_per_page' => 7,
		'offset' => 0,
		'category' => 0,
		'orderby' => 'post_date',
		'order' => 'DESC',
		// 'include' => '',
		// 'exclude' => '',
		// 'meta_key' => '',
		// 'meta_value' =>'',
		'post_type' => get_post_type(),
		'post_status' => 'publish',
		'suppress_filters' => true
	);

	$recent_posts = new WP_Query($args);
	// $recent_posts = wp_get_recent_posts( $args, ARRAY_A );

	?>
   <!-- pageheader
    ================================================== -->
   <?php get_header(); ?>

   <div class="colorlib-loader"></div>

   <div id="page">
   	<div class="container-fluid">
   		<div class="row">
   			<div class="col-md-12 bg-deraz-dark  top-about-section breadcrumbs text-center">
   				<h2 class="text-white bg-deraz" style="width: auto;padding: 1rem;"><?= ucwords(the_title()) ?></h2>
   				<?php echo bcn_display($return = false, $linked = true, $reverse = false, $force = false) ?>

   			</div>
   		</div>
   	</div>

   	<div id="colorlib-container">
   		<div class="container">
   			<div class="row">
   				<div class="col-md-9 content">
   					<div class="row row-pb-lg">
   						<?php
							if (have_posts()) :
								while (have_posts()) : the_post();
									?>
   						<div class="col-md-12">
   							<div class="blog-entry">

   								<div class="blog-img ">
   									<img src="<?= get_the_post_thumbnail_url(); ?>" class="img-fluid img-responsive" alt="">
   								</div>
   								<div class="desc">
   									<p class="meta">
   										<span class="cat">
   											<?php
														$categories = get_the_category();
														print_categories_as_tags($categories, 'featured-tag');
														?>
   										</span>
   										<span class="date"><?= the_date("Y-m-d"); ?></span>
   										<span class="pos"><?= __("by"); ?> <?= get_field("author"); ?></span>
   									</p>
   									<h2><?= the_title(); ?></h2>
   									<?php the_content(); ?>
   								</div>
   							</div>
   						</div>
   						<?php
								endwhile;
							endif;
							?>
					   </div>
					   <?php
						$types = ["post", "spotlight", "opportunity"];
						if (in_array(get_post_type(), $types)):  
						?>
					<div class="comments">
						<div class="row row-pb-lg">
							<?php $comments = get_comments([
									"post_id" => get_the_ID()
								]); ?>
							<div class="col-md-12">
								<h2 class="heading-2"><?= count($comments) ?> Comments</h2>
								<?php
									foreach ($comments as $cmt) :
										?>
								<div class="review">
									<div class="user-img" style="background-image: url(images/person1.jpg)"></div>
									<div class="desc">
										<h4>
											<span class="text-left"><?= $cmt->comment_author ?></span>
											<span class="text-right"><?= $cmt->comment_date ?></span>
										</h4>
										<?= $cmt->comment_content ?>
										<p class="star">
											<span class="text-left"><a href="#" class="reply"><i class="icon-reply"></i></a></span>
										</p>
									</div>
								</div>
								<?php endforeach ?>
							</div>

						</div>
						<div class="row">
							<div class="col-md-12">
								<?php
									comment_form();
								?>
							</div>
						</div>
					</div>
					<?php endif; ?>
   				</div>
   				<div class="col-md-3">
   					<div class="sidebar">
   						<div class="side">
   							<div class="form-group">
   								<input type="text" class="form-control" id="search" placeholder="Enter any key to search...">
   								<button type="submit" class="btn btn-primary"><i class="icon-search3"></i></button>
   							</div>
   						</div>
   						<!-- <div class="side">
								<h2 class="sidebar-heading">Categories</h2>
								<p>
									<ul class="category">
									<?php
									foreach ($all_categories as $cat) {
										echo '<li><a href="' . get_category_link($cat) . '"><i class="icon-check"></i>' . $cat->name . '</a></li>';
									}
									?>
									</ul>
								</p>
							</div> -->
   						<div class="side">
   							<h2 class="sidebar-heading">Recent posts</h2>
   							<div class="f-blog">
   								<?php
									if ($recent_posts->have_posts()) :
										while ($recent_posts->have_posts()) :
											$recent_posts->the_post();
											$bg =  wp_get_attachment_url(get_post_thumbnail_id());
											?>
   								<div class="desc my-3">
   									<h3>
   										<a href="<?= the_permalink(); ?>">
   											<div class="row">
   												<div class="col-lg-4">
   													<?= get_the_post_thumbnail(null, array('80', '80')); ?>
   												</div>
   												<div class="col-lg-8">
   													<?= the_title(); ?><br>
   													<small><?= the_date("Y-m-d"); ?></small>
   												</div>
   											</div>
   										</a>
   									</h3>
   									<hr>
   								</div>
   								<?php
										endwhile;
									endif;
									?>
   							</div>
   						</div>
   					</div>
   				</div>
   			</div>
   		</div>
   	</div>

   	<!-- s-footer
    ================================================== -->
   	<?php get_footer(); ?>