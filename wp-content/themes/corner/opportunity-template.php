<?php
/*
Template Name: Opportunity
*/
?>
<?php
$args = array(
    'post_type' => 'opportunity_pt',
    'posts_per_page' => 6,
    'paged' => $paged,
);

$opp_posts = new WP_Query($args);
// echo print_r($featured);
?>
<!-- pageheader
    ================================================== -->
<?php get_header(); ?>
<div class="bg-deraz-dark top-about-section">
    <h2 class="bg-deraz text-center"><?php the_title(); ?></h2>
</div>

<div class="colorlib-loader"></div>

<div id="page">

    <div id="colorlib-container">
        <div class="container-fluid col-lg-10 offset-lg-1">
            <?php
            $i = 0;
            if ($opp_posts->have_posts()) :
                while ($opp_posts->have_posts()) : $opp_posts->the_post();
                    if ($i % 2 === 0) {
                        echo '<div class="row row-pb-md">';
                    }
                    ?>
                    <div class="col-xl-6 col-lg-6 <?= ($i + 1) % 2 === 0 ? 'col-md' : 'col-md-6'; ?>">
                        <div class="blog-entry">
                            <div class="blog-img" style="max-height:20rem;">
                                <a href="<?= the_permalink() ?>"><?= the_post_thumbnail('large', array('class' => 'img-fluid')); ?></a>
                            </div>
                            <div class="desc">
                                <p class="meta">
                                    <span class="cat">
                                        <?php
                                        $categories = get_the_category();
                                        print_categories_as_tags($categories);
                                        ?>
                                    </span>
                                    <span class="date"><?= the_date("Y-m-d"); ?></span>
                                    <span class="pos"> By <a href="#"><?= the_author(); ?></a></span>
                                </p>
                                <h2 class="border-left-deraz pl-2"><a href="<?= the_permalink() ?>"><?= the_title(); ?></a></h2>
                                <div class="text-justify">
                                    <?= the_excerpt(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
                    if ($i % 2 === 0) {
                        echo "</div>";
                    }

                endwhile;
                ?>
            </div>
        </div>
    </div>
    <div class="clearfix">
        <div class="container">

            <?php
            echo bootstrap_pagination();
        endif;
        ?>
    </div>
</div>

<!-- s-footer
    ================================================== -->
<?php get_footer(); ?>