    <?php
    $featured =  new WP_Query(array('category_name' => 'featured'));
    // echo print_r($featured);
    ?>
    <!-- pageheader
    ================================================== -->
    <?php get_header(); ?>


    <div class="colorlib-loader"></div>

    <div id="page">

        <aside id="colorlib-hero">
            <div class="flexslider">
                <ul class="slides">
                    <?php
                    if ($featured->have_posts()) :
                        while ($featured->have_posts()) : $featured->the_post();
                            // print_r( $featured->get_the_post_thumbnail());
                            $bg =  wp_get_attachment_url(get_post_thumbnail_id());
                            // print_r($bg);
                            ?>
                            <li style="background-image: url(<?= $bg ?>);">
                                <div class="overlay"></div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-6 col-md-pull-3 col-sm-12 col-xs-12 col-md-offset-3 slider-text">
                                            <div class="slider-text-inner">
                                                <div class="desc">
                                                    <p class="meta">
                                                        <span class="cat">
                                                            <?php
                                                                    $categories = get_the_category();
                                                                    print_categories_as_tags($categories, 'featured-tag');
                                                                    ?>
                                                        </span>
                                                        <span class="date"><?= the_time() . " " . the_date("Y-m-d"); ?></span>
                                                        <span class="pos">By <a href="#"><?= the_author() ?></a></span>
                                                    </p>
                                                    <h1><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                    <?php
                        endwhile;
                    endif;
                    ?>
                </ul>
            </div>
        </aside>

        <div id="colorlib-container">
            <div class="container">
                <div class="row mb-5 mt-3">
                    <div class="push-top bg-deraz-dark text-center">
                        <?php if (pll_current_language() == "ar") : ?>
                            <h1 class="text-center d-inline-block title-style-deraz text-white">شاركنا بتدوينتك</h1>
                        <?php else : ?>
                            <h1 class="text-center d-inline-block title-style-deraz text-white"><b>Blog</b> us</h1>
                        <?php endif; ?>
                        <br>
                        <?php if (pll_current_language() == "ar") : ?>
                            <p class="text-center"> يمكنك أنت أيضاً مشاركتنا كتابتك الخاصة لتنشر على موقعنا، سنكون سعداء بإستقبال وقراءة ما تدون دائماً....</p>
                        <?php else : ?>
                            <p class="text-center"> you can also have your own blog on our website , so feel free to send us your blog anytime, we would be happy to read your writings!...</p>
                        <?php endif; ?>
                        <a class="d-inline-block flat-button-deraz" href="<?= get_page_url("contact-us") ?>">Contact Us</a>
                    </div>
                </div>
            </div>
            <div class="container-fluid col-lg-10 offset-lg-1">
                <?php
                $i = 0;
                if (have_posts()) :
                    while (have_posts()) : the_post();
                        if ($i % 3 === 0) {
                            echo '<div class="row row-pb-md">';
                        }
                        ?>
                        <div class="col-xl-4 col-lg-6 <?= ($i + 1) % 3 === 0 ? 'col-md' : 'col-md-6'; ?>">
                            <div class="blog-entry">
                                <div class="blog-img" style="max-height:20rem;">
                                    <a href="<?= the_permalink() ?>"><?= the_post_thumbnail('large', array('class' => 'img-fluid')); ?></a>
                                </div>
                                <div class="desc">
                                    <p class="meta">
                                        <span class="cat">
                                            <?php
                                                    $categories = get_the_category();
                                                    print_categories_as_tags($categories);
                                                    ?>
                                        </span>
                                        <span class="date"><?= the_date("Y-m-d"); ?></span>
                                        <span class="pos">By <a href="#"><?= the_author(); ?></a></span>
                                    </p>
                                    <h2 class="border-left-deraz pl-2"><a href="<?= the_permalink() ?>"><?= the_title(); ?></a></h2>
                                    <div class="text-justify">
                                        <?= the_excerpt(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                            $i++;
                            if ($i % 3 === 0) {
                                echo "</div>";
                            }

                        endwhile;
                        ?>
            </div>
        </div>
    </div>
    <div class="clearfix">
        <div class="container">

        <?php
            echo bootstrap_pagination();
        endif;
        ?>
        </div>
    </div>

    <!-- s-footer
    ================================================== -->
    <?php get_footer(); ?>