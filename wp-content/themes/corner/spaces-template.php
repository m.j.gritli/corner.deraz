<?php
/*
Template Name: Spaces
*/
?>
<?php get_header(); ?>
<!-- Page Body  -->
<div class="bg-deraz-dark top-about-section">

  <h2 class="bg-deraz text-center">Spaces</h2>

</div>
<div>
  <!-- row 1 -->
  <div style="background-image: url(<?php echo get_bloginfo('template_directory'); ?>/images/arrows_right.svg); background-size: cover; background-attachment: fixed;">
    <h2 class="section-header display-4 bg-deraz-dark ">Co-Working <b>Space</b></h2>

    <div class="row space-plans">
      <?php

      $plans_req  = wp_remote_get(get_api_url("plans"));
      $plans = json_decode($plans_req["body"])->data;
      $plans_count = count($plans);
      // print_r($plans);
      if ($plans_count) :
        echo "<style>";

        echo ".plan-icon {";
        echo "fill: black;";
        echo "  display: inline-block;";
        echo "  width: auto;";
        echo "  height: 3.5rem;";
        echo "  background-size: cover;";
        echo "  background-repeat: no-repeat;";
        echo "   filter: invert(100%);";
        echo "}";
        foreach ($plans as $plan) :
          echo ".plan-" . $plan->slug . " {";
          // echo "background-image: url($plan->icon);";
          echo "}";
        endforeach;
        echo "</style>";
      endif;
      foreach ($plans as $plan) :
        ?>
        <!-- plan col  -->
        <div class="col-lg-3 col-md-6 col-sm-12 m-0 p-2 plan">
          <div class="bg-deraz plan-block plan-block-grid">
            <!-- <img src="<?php echo get_bloginfo('template_directory'); ?>/images/one-day.svg" alt="" height="50"> -->
            <?php $name = "name_" . pll_current_language(); ?>
            <img class="plan-icon plan-<?= $plan->slug; ?>" src="<?= $plan->icon ?>" alt="">
            <h3 class="text-white">
              <?= ucwords($plan->$name); ?>
            </h3>
            <hr class="thick-border-bottom ">
            <ul>
              <?php foreach ($plan->services as $service) : ?>
                <li>
                  <!-- <i class="fa fa-<?= $service->icon; ?>"></i> -->
                  <i class="fas fa-<?= $service->icon; ?>"></i>
                  <!-- <img src="<?php echo get_bloginfo('template_directory'); ?>/images/one-day.svg" alt="" height="20"> -->
                  <p>
                    <?= ucwords($service->$name); ?>
                  </p>
                </li>
              <?php endforeach; ?>
              <li><i class="fas fa-tags"></i> <?= __("prices"); ?>
                <ul style="list-style: circle; margin-left:2rem;">
                  <?php foreach ($plan->prices as $price) : ?>
                    <li>
                      <b><?= ucwords($price->$name); ?>: <?= $price->price; ?> LYD</b>
                    </li>
                  <?php endforeach; ?>
                </ul>
              </li>
            </ul>
          </div>
          <div class="bg-deraz plan-block">
            <h5><?= __("join-us") ?></h5>
          </div>
        </div>
      <?php endforeach; ?>

    </div>


  </div>

  <div class="d-none d-lg-block bg-arrows-right-green" style=" background-attachment: fixed; height:6rem;">
  </div>
  <!-- row 2  -->
  <div class="bg-arrows-right-green">
    <div class="row">
      <div class="col-lg-6 d-none d-lg-block" style="	background-image: url(path/here)">
        <img src="url(path/here" class="block-image" alt="">
      </div>
      <div class="col-lg-6 p-5">
        <h1 class="display-4 pb-3 bg-white">Training & Meeting Room <br></h1>
        <p class="bg-white p-2 mb-0">Renting the space for meetings, seminars, and training
          Capacity of 12- 15 seats, with different layout options, rent is 50 Lyd per hour <br>
          Included embities:
          <ul class="bg-white mt-0 pb-2">
            <li>Chairs and table </li>
            <li>Layout options</li>
            <li>Coffee , tea & water </li>
            <li>Speed internet / 2Gb free </li>
            <li>Printing service </li>
            <li>Office supplies </li>
            <li>Projector</li>
            <li>Flip chart</li>
            <li>Whiteboard</li>
          </ul>
        </p>
      </div>
      <div class="col-lg-6 d-block d-lg-none d-xl-none" style="	background-image: url(path/here);">
        <img src="url(path/here);" class="block-image" alt="">

      </div>

    </div>
  </div>
  <!-- row 3 -->
  <div class="bg-deraz-dark bg-arrows-right-green">
    <div class="row">
      <div class="col-lg-6 p-5">
        <h1 class="display-4 pb-3 text- p-2 mb-0white bg-deraz-dark">Event Space<br></h1>
        <p class="bg-deraz-dark p-2 mb-0">Rent the space for special event, meetups, discussions, conference the price is 100 per hour, seats 20-35 people

          <br>
          <p class="bg-deraz-dark"> Included embities:</p>
          <ul class="bg-deraz-dark mt-0 pb-2">
            <li>Chairs and table </li>
            <li>Layout options</li>
            <li>Coffee , tea & water </li>
            <li>Speed internet / 2Gb free </li>
            <li>Printing service </li>
            <li>Office supplies </li>
            <li>Projector</li>
            <li>Flip chart</li>
            <li>Whiteboard</li>
          </ul>

        </p>
      </div>
      <div class="col-lg-6" style="	background-image: url(/images/office.webp);">
        <img src="url(/images/office.webp);" class="block-image" alt="">
      </div>
    </div>
  </div>

</div>
<script>

</script>
<?php get_footer(); ?>