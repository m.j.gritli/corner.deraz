 
   <?php
	$all_categories = get_categories();
	
	$args = array(
		'posts_per_page' => 7,
		'offset' => 0,
		'category' => 0,
		'orderby' => 'post_date',
		'order' => 'DESC',
		'post_type' => 'spotlight_pt',
		'post_status' => 'publish',
		'suppress_filters' => true
	);
	
	$recent_posts = new WP_Query( $args, ARRAY_A );
	// $recent_posts = wp_get_recent_posts( $args, ARRAY_A );

   ?>
    <!-- pageheader
    ================================================== -->
    <?php get_header(); ?>

	<div class="colorlib-loader"></div>

	<div id="page">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 bg-deraz-dark  top-about-section breadcrumbs text-center">
						<h2 class="text-white bg-deraz" style="width: auto;padding: 1rem;"><?= ucwords(the_title())?></h2>
						<p class="text-white"><span><a href="<?= get_bloginfo('url'); ?>">Home</a></span> / <span><a href="/community/spotlight"><?= __('Spotlights') ?> </a></span> / <span><?= the_title()?></span></p>
					</div>
				</div>
			</div>

		<div id="colorlib-container">
			<div class="container">
				<div class="row">
					<div class="col-md-9 content">
						<div class="row row-pb-lg">
						<?php
						 	if (have_posts()) :
								while (have_posts()) : the_post(); 
						?>
							<div class="col-md-12">
								<div class="blog-entry">
									<div class="blog-img blog-detail">
										<img src="<?= get_the_post_thumbnail(); ?>" class="img-responsive">
									</div>
									<div class="desc">
										<p class="meta">
											<span class="cat">
											<?php
												$categories = get_the_category();
												print_categories_as_tags($categories,'featured-tag');
											?>
											</span>
											<span class="date"><?= the_time() . " " . the_date("Y-m-d"); ?></span>
											<span class="pos">By <a href="#"><?= the_author(); ?></a></span>
										</p>
										<h2><a href="blog.html"><?= the_title(); ?></a></h2>
										<?php the_content();?>
									</div>
								</div>
							</div>
						<?php
						       endwhile;
							endif;
						?>
						</div>
						
					</div>
					<div class="col-md-3">
						<div class="sidebar">
							<div class="side">
								<div class="form-group">
									<input type="text" class="form-control" id="search" placeholder="Enter any key to search...">
									<button type="submit" class="btn btn-primary"><i class="icon-search3"></i></button>
								</div>
							</div>
							<!-- <div class="side">
								<h2 class="sidebar-heading">Categories</h2>
								<p>
									<ul class="category">
									<?php 
										foreach($all_categories as $cat){
											echo '<li><a href="'.get_category_link($cat).'"><i class="icon-check"></i>'.$cat->name.'</a></li>';
										} 
									?>
									</ul>
								</p>
							</div> -->
							<div class="side">
								<h2 class="sidebar-heading"><?= __('recent-spotlights') ?> </h2>
								<div class="f-blog">
								<?php 
								if($recent_posts->have_posts()):
									while($recent_posts->have_posts()):
										$recent_posts->the_post();
										$bg =  wp_get_attachment_url( get_post_thumbnail_id());
									?>
											<div class="desc my-3">
												<h3>
													<a href="<?= the_permalink();?>">
														<div class="row">
															<div class="col-lg-4">
																<?= get_the_post_thumbnail(null,array('80','80'));?>
															</div>
															<div class="col-lg-8">
																<?= the_title();?><br>
																<small><?= the_date("Y-m-d");?></small>
															</div>
														</div>
													</a>
												</h3>
												<hr>
											</div>
									<?php 
									endwhile;
								endif;
								?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>					

        <!-- s-footer
    ================================================== -->
	<?php get_footer(); ?>

