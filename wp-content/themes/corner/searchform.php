<form class="form" method="get" action="<?php bloginfo('url'); ?>">
  <h2><?php echo pll_e("Search"); ?></h2>
    <input type="search" class="form-control" name="s" value="<?php the_search_query(); ?>" placeholder="<?php echo pll_e("Type here to search ..."); ?>" />
    <!-- <br> -->
    <!-- <div class="clearfix"></div> -->
  <div class="text-center">
      <button type="submit" name="search_wp" class="btn btn-primary"><?php echo pll_e("Search"); ?></button>
  </div>
</form>
