<!DOCTYPE html>
<html lang="<?= function_exists("pll_current_language")  ? pll_current_language() : "ar"; ?>">

<head>
  <title>
    <?php
    if (is_page('home')) {
      $title = get_bloginfo('name') . ' | ' . get_bloginfo('description');
      //   $title = get_bloginfo('name') . ' | Home';
    } elseif (is_home()) {
      $title = get_bloginfo('name') . ' | Blog';
    } elseif (is_singular('projects')) {
      $title = wp_title(get_bloginfo('name') . ' | Projects - ');
    } else {
      $title = bloginfo('name') . wp_title(' | ');
    }
    echo $title;
    ?>
  </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />

  <!-- socials -->
  <meta property=" fb:app_id" content="1401659709870300" />
  <meta property="fb:pages" content="488606014647960" />
  <meta property="og:url" content="<?php echo get_bloginfo('url'); ?>" />
  <meta property="og:type" content="page" />
  <meta property="og:title" content="<?php echo $title; ?>" />
  <meta property="og:site_name" content="<?php bloginfo('name'); ?>" />
  <?php
  if (get_the_post_thumbnail_url()) {
    $image = get_the_post_thumbnail_url();
  } else {
    $image = get_bloginfo('template_directory') . '/images/websers-logo-large.png';
  }
  ?>
  <meta property="og:image" content="<?php echo $image; ?>" />



  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/fonts/icomoon/style.css">
  <!-- plugins files -->
  <?php wp_head(); ?>
  <!-- /plugins files -->
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/bootstrap.min.css">
  <?php if (function_exists("pll_current_language") && pll_current_language() == "ar") : ?>
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/bootstrap-rtl.min.css">
  <?php endif; ?>
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/animate.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/owl.theme.default.min.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/bootstrap-datepicker.css">

  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/fonts/flaticon/font/flaticon.css">

  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/aos.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/jquery.fancybox.min.css">


  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/fontawesome.css">

  <link href="https://vjs.zencdn.net/7.6.0/video-js.css" rel="stylesheet">

  <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
  <script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>



  <!-- events -->
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/events.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/events_responsive.css">
  <!-- events -->

  <!-- Animate.css -->
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/blog/animate.css">
  <!-- Icomoon Icon Fonts-->
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/blog/icomoon.css">
  <!-- Bootstrap  -->
  <!-- <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/blog/bootstrap.css"> -->

  <!-- Magnific Popup -->
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/blog/magnific-popup.css">

  <!-- Flexslider  -->
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/blog/flexslider.css">

  <!-- Owl Carousel -->
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/blog/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/blog/owl.theme.default.min.css">

  <!-- icons -->
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/iconmoon.css">
  <!-- Blog Theme style  -->
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/blog/style.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/bootstrap-override.css">

  <!-- Theme style  -->
  <link href="https://fonts.googleapis.com/css?family=Tajawal:400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/main.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/overlay.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/flipcards.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/style.css">
  <!-- overrides -->
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/lg-override.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/md-override.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/sm-override.css">

  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/style.css">



  <!-- Modernizr JS -->
  <script src="<?php echo get_bloginfo('template_directory'); ?>/js/blog/modernizr-2.6.2.min.js"></script>

  <?php if (function_exists("pll_current_language") && pll_current_language() == "ar") : ?>
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/style-rtl.css">
  <?php endif; ?>
</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->

    <div class="site-navbar-wrap ">


      <div class="site-navbar bg-deraz-dark site-navbar-target  fixed-top">
        <div class="container-fluid">
          <div class="row align-items-center">

            <div class="col-6 col-md-5 col-sm-7 logo">
              <a href="<?= get_bloginfo("url"); ?>"><img class="site-logo img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/images/logo.svg" alt="DERAZ CORNER"></a>
              <a href="#" onclick="openSearch()"><img class="site-search img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/images/search.svg" alt="DERAZ CORNER"></a>
            </div>
            <div class="col-6 col-md-7 col-sm-5">





              <nav class="site-navigation text-right" role="navigation">
                <div class="container">
                  <!--class= 'js-menu-toggle' -->
                  <!-- <div class="d-inline-block d-lg-block ml-md-0 mr-auto py-3"><a href="#"  class="site-menu-toggle js-menu-toggle text-black"> -->
                  <div class="d-inline-block d-lg-block ml-md-0 mr-auto">

                    <ul class="menu">
                      <?php
                      pll_the_languages(
                        array(
                          // 'show_flags'=>1,
                          'show_names' => 1
                        )
                      );
                      ?>

                      <!-- <li> -->
                      <a href="#" onclick="openNav()" class="site-menu-toggle text-black">
                        <img src="<?php echo get_bloginfo('template_directory'); ?>/images/menu.svg" alt="">
                      </a>
                      <!-- </li> -->
                    </ul>
                  </div>

                  <ul class="site-menu main-menu js-clone-nav d-none d-lg-none">
                    <li><a href="#home-section" class="nav-link">Home</a></li>
                    <li><a href="#about-section" class="nav-link">About Us</a></li>
                    <li><a href="#what-we-do-section" class="nav-link">What We Do</a></li>
                    <li><a href="#portfolio-section" class="nav-link">Portfolio</a>
                      <ul class="drop-down collapsed">
                        <li><a href="#portfolio-section" class="nav-link">Portfolio</a>
                        <li><a href="#portfolio-section" class="nav-link">Portfolio</a>
                        <li><a href="#portfolio-section" class="nav-link">Portfolio</a>
                      </ul>
                    </li>
                    <li><a href="#contact-section" class="nav-link">Contact</a></li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- END .site-navbar-wrap -->

    <!-- <nav class="navbar navbar-expand-sm navbar-dark fixed-top ">
      
      <div class="logo">
        <a href="<?= get_bloginfo("url"); ?>"><img class="site-logo img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/images/logo.svg" alt="DERAZ CORNER"></a>
        <a href="#" onclick="openSearch()"><img class="site-search img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/images/search.svg" alt="DERAZ CORNER"></a>
      </div>
      <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
          aria-expanded="false" aria-label="Toggle navigation"></button>
      <div class="collapse navbar-collapse" id="collapsibleNavId">
          <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            
          </ul>
          <ul class="menu d-inline my-2 my-lg-0">
             
              <?php
              //  pll_the_languages();
              ?>
              <li>
                  <a href="#" onclick="openNav()" class="site-menu-toggle text-black">
                    <img src="<?php echo get_bloginfo('template_directory'); ?>/images/menu.svg" alt="">
                  </a>
              </li>
          </ul>
      </div>
  </nav> -->

    <nav id="myNav" class="overlay">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
      <a href="javascript:void(0)" class="logo" onclick="closeNav()"><img src="<?php echo get_bloginfo('template_directory'); ?>/images/logo-sm.svg" class="img-fluid" alt="Dc"></a>
      <div class="overlay-content">
        <div class="container">
          <div class="col-lg-6 offset-lg-3 text-left">
            <?php
            // wp_nav_menu( array( 'theme_location' => 'main' ) );
            ?>
            <?php
            /* Primary navigation */
            wp_nav_menu(
              array(
                'menu' => 'home menu',
                'theme_location' => 'main',
                'depth' => 2,
                'container' => false,
                'menu_class' => 'nav flex-column flex-nowrap',
                //Process nav menu using our custom nav walker
                // 'walker' => new wp_bootstrap_navwalker()
              )
            );
            ?>
          </div>
        </div>
      </div>
    </nav>
    <div id="mySearch" class="overlay">
      <a href="javascript:void(0)" class="closebtn" onclick="closeSearch()">&times;</a>
      <a href="javascript:void(0)" class="logo" onclick="closeSearch()"><img src="<?php echo get_bloginfo('template_directory'); ?>/images/logo-sm.svg" class="img-fluid" alt="Dc"></a>
      <div class="overlay-content">
        <div class="container">
          <div class="col-xs-10 col-xs-offset-1 search-form">

            <form action="<?php bloginfo("url"); ?>" method="get">

              <input type="text" name="s" id="search" class="form-control" placeholder="Hello search me" value="<?php the_search_query(); ?>" />
              <!-- <button type="submit"><i class="fa fas-search"></i></button> -->
              <!-- <input type="image" alt="Search" src="<?php bloginfo('template_url'); ?>/images/search.png" /> -->
            </form>
          </div>
        </div>
      </div>
    </div>