<?php
/*
Template Name: Community
*/
?>

<?php
$all_categories = get_categories();

$spotlight_args = array(
    'post_type' => 'spotlight_pt',
    'posts_per_page' => 3,
);

$opportunity_args = array(
    'post_type' => 'opportunity_pt',
    'posts_per_page' => 3,
);

$review_args = array(
    'post_type' => 'review',
    'posts_per_page' => 3,
);

$spotlight_posts = new WP_Query($spotlight_args, ARRAY_A);
$opportunity_posts = new WP_Query($opportunity_args, ARRAY_A);
$review_posts = new WP_Query($review_args, ARRAY_A);

?>
<?php get_header(); ?>
<div class="bg-deraz-dark top-about-section">
    <h2 class="bg-deraz text-center"><?php the_title(); ?></h2>
</div>
<div class="container-fluid mt-5 py-5 ">
    <!-- <div class="row mt-2">
        <div class="col-md-12 bg-deraz-dark p-4">
          <h2 class="text-light">Welcome to Our ...<br></h2>
        </div>
      </div> -->

    <div class="container">
        <div class="row">
            <div class="push-top bg-deraz-dark text-center">
                <?php if (pll_current_language() == "ar") : ?>
                    <h1 class="text-center d-inline-block title-style-deraz text-white">ماهو <b>مجتمع دراز ?</b></h1>
                <?php else : ?>
                    <h1 class="text-center d-inline-block title-style-deraz text-white">What is Our <b>Community ?</b></h1>
                <?php endif; ?>
                <br>
                <?php if (pll_current_language() == "ar") : ?>

                    <p class="text-center">دراز كورنرهي بيئة شابة، ملهمة وداعمة، فنعمل في دراز على تشجيع المواهب والكفاءات الشابة لتقديم أفضل ما لديهم وتشجيعهم على العمل وتحقيق أهدافهم من خلال تهيئة البيئة المناسبة لهم ودمجهم بغيرهم لتكوين صداقات ومجتمع يشاركهم شغفهم وطموحاتهم. يتكون مجتمع الكورنر من أعضاء يدعم بعضهم البعض، يتشاركون الشغف والتشجيع والعمل كذلك.
                        ...</p><a class="d-inline-block flat-button-deraz" href="<?= get_page_url("contact-us") ?>">Contact Us</a>
                <?php else : ?>
                    <p class="text-center">Deraz Corner is a youthful, inspiring environment. At Deraz we work on supporting young talents to achieve their goals by providing them the appropriate place to help them make friends, and build a community they can share passion and ambitions with. Deraz society contains members share together passion, encouragement, and work as well....</p>
                    <a class="d-inline-block flat-button-deraz" href="<?= get_page_url("contact-us") ?>">Contact Us</a>

                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid my-5 community-green-bg">
    <div class="row">
        <h2 class="display-4 p-3 ml-3 mt-5 border-left-deraz bg-white">
            <?= __('Spotlights') ?>
        </h2>
    </div>
    <div class="row mt-5">
        <?php
        if ($spotlight_posts->have_posts()) :
            $i = 0;
            while ($spotlight_posts->have_posts()) : $spotlight_posts->the_post(); ?>

                <div class="col-12 col-lg-4">

                    <div class="blog-entry shadow">
                        <div class="blog-img" style="max-height:20rem;">
                            <a href="<?= the_permalink() ?>"><?= the_post_thumbnail('large', array('class' => 'img-fluid')); ?></a>
                        </div>
                        <div class="desc">
                            <h2><a href="<?= the_permalink() ?>"><?= the_title(); ?></a></h2>
                            <p class="meta">
                                <span class="date"><?= the_date("Y-m-d"); ?></span>
                            </p>

                            <?= the_excerpt(); ?>
                        </div>
                    </div>
                </div>

        <?php
                $i++;
            endwhile;
            wp_reset_postdata();
        endif;
        wp_reset_query();
        ?>
    </div>
    <div class="row mt-4">
        <h3 style="margin:2.5rem auto;">

            <a href="<?php echo get_page_url(54); ?>" class="view-more-button"><?= __('view-more-spotlights->') ?></a>
        </h3>
    </div>
</div>

<div class="container-fluid my-5 community-dark-bg">
    <div class="row">
        <h2 class="display-4 p-3 ml-3 mt-5 border-left-deraz bg-white">
            <?= __('Opportunities') ?>
        </h2>
    </div>
    <div class="row mt-5">
        <?php

        if ($opportunity_posts->have_posts()) :
            $i = 0;
            while ($opportunity_posts->have_posts()) : $opportunity_posts->the_post(); ?>

                <div class="col-12 col-lg-4">

                    <div class="blog-entry shadow">
                        <div class="blog-img" style="max-height:20rem;">
                            <a href="<?= the_permalink() ?>"><?= the_post_thumbnail('large', array('class' => 'img-fluid')); ?></a>
                        </div>
                        <div class="desc">
                            <p class="meta">
                                <span class="cat">
                                    <?php
                                            $categories = get_the_category();
                                            print_categories_as_tags($categories);
                                            ?>
                                </span>
                                <span class="date"><?= the_date("Y-m-d"); ?></span>
                                <span class="pos">By <a href="#"><?= the_field("author"); ?></a></span>
                            </p>
                            <h2><a href="<?= the_permalink() ?>"><?= the_title(); ?></a></h2>
                            <?= the_excerpt(); ?>
                        </div>
                    </div>
                </div>

        <?php

            endwhile;
            wp_reset_postdata();
        endif;
        wp_reset_query();
        ?>
    </div>
    <div class="row mt-4">
        <h3 style="margin:2.5rem auto;">
            <a class="view-more-button" href="<?php echo get_page_url(56); ?>"><?= __('view-more-opportunities->') ?></a>
        </h3>
    </div>
</div>

<div class="container-fluid my-5 community-green-bg">
    <div class="row">
        <h2 class="display-4 p-3 ml-3 mt-5 border-left-deraz bg-white">
            <?= __('Reviews') ?>
        </h2>
    </div>
    <div class="row mt-5">
        <?php
        if ($review_posts->have_posts()) :
            $i = 0;
            while ($review_posts->have_posts()) : $review_posts->the_post(); ?>

                <div class="col-lg-4 col-md-12">

                    <div class="blockquote mb-0 bg-white p-3 my-3 shadow">
                        <h3 class="review-title"><?= ucwords(get_the_title()); ?></h3><br> <?php print_stars(get_field('stars')) ?>
                        <p class="border-left-deraz-qoute" style="min-height:221px">"<?= get_the_excerpt() ?>"</p>
                        <div class="blockquote-footer" style="min-height:40px;">
                            <img class="img-fluid d-inline-block rounded-circle mx-2" src="<?= get_field("author_image") ?>"><b><?= get_field('author') ?></b></div>
                    </div>
                </div>

        <?php
                $i++;
            endwhile;
            wp_reset_postdata();
        endif;
        wp_reset_query();

        ?>
    </div>
    <div class="row mt-4">
        <h3 style="margin:2.5rem auto;">
            <a class="view-more-button" href="<?php echo get_page_url(59); ?>"><?= __('view-more-reviews->') ?></a>
        </h3>
    </div>
</div>

<?php get_footer(); ?>