<?php get_header(); ?>

<div class="conrainer-fluid">

  <div class="bg-white about-container">
    <div class="row">
      <div class="col-lg-6 br">
        <h4>What we do ?</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam sequi vel, doloremque labore ea accusamus
          hic mollitia perspiciatis earum facere ex magnam eaque id, inventore temporibus officia magni sit odit.</p>
      </div>
      <div class="col-lg-6">
        <h4>Who we are?</h4>
        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ut laboriosam voluptatem aliquam recusandae,
          consequatur corrupti esse, quaerat aut exercitationem fuga dolorem magni aspernatur voluptas vitae
          voluptates delectus facere, libero illo?</p>
      </div>
    </div>
  </div>

  <div class="bg-white grid-section">
    <div class="row">
      <div class="col-lg-5 left-block">
        <div class="display-image" style="background-image: url(images/grey-lg.svg)">
          <img src="images/grey-lg.svg" style="opacity:0;" alt="">
        </div>
        <div class="row">
          <div class="col-lg-6">
            <h3>
              Creative <br />Co-working<br /> Space
            </h3>
          </div>
          <div class="col-lg-6">
            <p class="grid-section-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum, odit quasi! Hic culpa, fuga, dolores,
              tempore eligendi magnam nam delectus beatae pariatur laudantium reprehenderit esse quod distinctio
              consectetur cum! Nostrum?</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3" style="background-image: url(images/green.svg)">
        <div class="border-white-bottom">
          <h1>+50</h1>
          <h2>User / Day</h2>
        </div>
        <div class="center-block">
          <h3>Students freelancers artists and you</h3>
        </div>
      </div>
      <div class="col-lg-4 right-block">
        <div class="display-image" style="background-image: url(images/grey-lg.svg)">
          <img src="images/grey-md.svg" style="opacity: 0;" alt="">
        </div>
        <div class="row">
          <div class="col-lg-6">
            <h3>
              Research & Development<br /> Space
            </h3>
          </div>
          <div class="col-lg-6">
            <p class="grid-section-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum, odit quasi! Hic culpa, fuga, dolores,
              tempore eligendi magnam nam delectus beatae pariatur?</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="bg-white grid-section">
    <div class="row">
      <div class="col-lg-5 left-block">
        <div class="display-image" style="background-image: url(images/blue-lg.svg)">
          <img src="images/blue-lg.svg" style="opacity: 0;" alt="">
        </div>
        <div class="row">
          <div class="col-lg-6">
            <h3>
              Creative <br />Co-working<br /> Space
            </h3>
          </div>
          <div class="col-lg-6">
            <p class="grid-section-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum, odit quasi! Hic culpa, fuga, dolores,
              tempore eligendi magnam nam delectus beatae pariatur laudantium reprehenderit esse quod distinctio
              consectetur cum! Nostrum?</p>
          </div>
        </div>
      </div>
      <div class="col-lg-7 right-block bg-deraz-dark">
        <div class="display-image" style="background-image: url(images/grey-lg.svg)">
          <img src="images/grey-md.svg" style="opacity: 0;" alt="">
        </div>
        <div class="row">
          <div class="col-lg-6 ">
            <h3 class="text-white">
              Research & Development<br /> Space
            </h3>
          </div>
          <div class="col-lg-6">
            <p class="grid-section-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum, odit quasi! Hic culpa, fuga, dolores,
              tempore eligendi magnam nam delectus beatae pariatur?</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="grid-section">
    <div class="row">
      <div class="col-lg-3 bg-deraz bottom-shadow-inset">
        <div class="border-white-bottom">
          <h1>+300</h1>
          <h2>Workshops & Events</h2>
        </div>
        <div class="center-block">
          <h3>Students freelancers artists and you</h3>
        </div>
      </div>
      <div class="col-lg-9 right-block">
        <div class="display-image" style="background-image: url(images/grey-lg.svg)">
          <img src="images/grey-md.svg" style="opacity: 0;" alt="">
        </div>
        <div class="row">
          <div class="col-lg-4">
            <h3>
              Netwroking & Events
            </h3>
          </div>
          <div class="col-lg-8">
            <p class="grid-section-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum, odit quasi! Hic culpa, fuga, dolores,
              tempore eligendi magnam nam delectus beatae pariatur?</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>