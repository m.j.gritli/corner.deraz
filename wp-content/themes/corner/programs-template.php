<?php
/*
Template Name: Programs
*/
?>

<?php
$all_categories = get_categories();

$args = array(
	'post_type' => 'program',
	'posts_per_page' => 3,
	'paged' => $paged,
);

$spotlight_posts = new WP_Query($args, ARRAY_A);
// echo'<pre>';
// 	print_r($spotlight_posts);
// echo'</pre>';

// $recent_posts = wp_get_recent_posts( $args, ARRAY_A );

?>

<!-- pageheader
    ================================================== -->
<?php get_header(); ?>
<div class="bg-deraz-dark top-about-section">
	<h2 class="bg-deraz text-center"><?php the_title(); ?></h2>
</div>
<div id="colorlib-container">

<?php
	if ($spotlight_posts->have_posts()) :
		$i = 1;
		while ($spotlight_posts->have_posts()) : $spotlight_posts->the_post();
			?>
			<?php if ($i % 2 == 0) : ?>
			<div class="container-fluid bg-deraz-dark py-4" style="background-image: url(<?php echo get_bloginfo('template_directory'); ?>/images/Component1.svg);background-size: cover;background-repeat: no-repeat;">
				<div class="container my-5 py-4" >
					<div class="row bg-deraz-dark">
						<div class="col-lg-6">
							<a href="<?= the_permalink() ?>">
								<?= get_the_post_thumbnail($post, 'full', array('class' => 'img-fluid img-responsive')); ?>
							</a>
						</div>
						<div class="col-lg-6 text-white">
							<a href="<?= the_permalink() ?>">
								<h1 class="display-4 border-left-deraz pl-4 text-white">
									<?= the_title() ?>
								</h1>
							</a>
							<?= the_excerpt(); ?>
						</div>
					</div>
				</div>
			</div>			
			<?php else : ?>
			<div class="container-fluid bg-white py-4">
				<div class="container my-5 py-4" style="background-image: url(<?php echo get_bloginfo('template_directory'); ?>/images/com1-dark-flipped.png);background-size: contain;background-repeat: no-repeat;">
					<div class="row bg-white">
						<div class="col-lg-6">
							<a href="<?= the_permalink() ?>">
								<h1 class="display-4 border-left-deraz pl-4">
									<?= the_title() ?>
								</h1>
							</a>
							<?= the_excerpt(); ?>
						</div>
						<div class="col-lg-6">
							<a href="<?= the_permalink() ?>">
								<?= get_the_post_thumbnail($post, 'full', array('class' => 'img-fluid img-responsive')); ?>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php 
			endif ;
			$i++;
			endwhile;
			?>
	<div class="clearfix">
		<div class="container">
			
			<?php
			echo bootstrap_pagination($spotlight_posts);
		endif;
		
		?>
		</div>
	</div>

</div>
<!-- s-footer
    ================================================== -->
<?php get_footer(); ?>