<!-- User has free key entered and should claim their free key -->
<div class="envato-claim-message">
	<p>
		<?php _e( 'Thank you for your Envato Market purchase. You currently have a Free Connect Key entered,
		but your Envato purchase entitles you to a Premium Connect Key.' ) ?>
	</p>
	<p>
		<?php printf( __( 'Please visit %sBoldGrid Central%s
		to link your accounts and claim your Premium Connect Key.' ),
		'<a target="_blank" href="https://www.boldgrid.com/central/code/envato">',
		'</a>' ) ?>
	</p>
</div>
