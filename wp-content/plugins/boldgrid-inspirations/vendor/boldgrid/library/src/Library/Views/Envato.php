<div class="envato-claim-message"><p>
	<?php printf(
		esc_html__(
			'Thank you for your Envato Market purchase.%sPlease visit %sBoldGrid Central%s to link your accounts and claim your Premium Connect Key.',
			'boldgrid-inspirations'
		),
		'</p><p>',
		'<a target="_blank" href="https://www.boldgrid.com/central/code/envato">',
		'</a>'
	); ?>
</p></div>
